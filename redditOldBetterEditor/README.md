[← Homepage](../README.md)

# Reddit Old Better Editor

Syntax highlighting & preview for [Reddit Old](https://old.reddit.com) post & comment editor.

| [Install](../../../../raw/branch/master/redditOldBetterEditor/main.user.js) |
|-----------------------------------------------------------------------------|

| ![](./screenshot1.png) | ![](./screenshot2.png) |
|------------------------|------------------------|

## Changelog

* `0.1.0` (2023-02-05) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/093ccba5444d6aeb467eb69166d27b2897e14422/redditOldBetterEditor)
* `0.1.1` (2023-02-28) • [Fix editor appearing in chat](https://git.kaki87.net/KaKi87/userscripts/src/commit/3d10fbadc5526020c993981569bf5bd83a50c006/redditOldBetterEditor)
[← Homepage](../README.md)

# YouTube Click To Play *(from [knoajp](https://github.com/knoajp))*

Modified to [add mobile support](https://git.kaki87.net/KaKi87/userscripts/commit/bede96b5cacd9d00285807d4d991d99a30150509).

| [Install](../../../../raw/branch/master/youtubeClickToPlay/main.user.js) |
|--------------------------------------------------------------------------|

- [Original README](./README.orig.md)
- [Original script](https://git.kaki87.net/KaKi87/userscripts/src/commit/79c5e5fd5f37d10bf8f5dd274ab3647bc0cf4be2/youtubeClickToPlay/index.user.js)
- [Original script on GreasyFork](https://greasyfork.org/en/scripts/406420-youtube-click-to-play)
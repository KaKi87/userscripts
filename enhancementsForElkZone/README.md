[← Homepage](../README.md)

# Enhancements for Elk Zone

| [Install](../../../../raw/branch/master/enhancementsForElkZone/main.user.js) |
|------------------------------------------------------------------------------|

## Features

- Always fetch post ancestors & descendants from remote instance
- Hide reposts everywhere (feature flag, disabled by default)
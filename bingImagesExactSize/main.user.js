// ==UserScript==
// @match       https://www.bing.com/images/search
// @name        Bing images search by exact size
// @description Search images by exact size on bing
// @version     0.1.0
// @author      KaKi87
// @license     WTFPL
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/bingImagesExactSize
// ==/UserScript==

const [, width, height] = new URL(window.location.href).searchParams.get('qft')?.match(/\bimagesize-custom_([0-9]+)_([0-9]+)\b/) || [];

if(width && height){
    const doFilterImages = () => document.querySelectorAll('[data-idx]').forEach(element => {
        if(!element.querySelector(`.img_badges [data-tooltip="${width} x ${height}"]`))
            element.remove();
    });
    doFilterImages();
    new MutationObserver(doFilterImages).observe(
        document.querySelector('#mmComponent_images_2'),
        { attributes: true }
    );
}
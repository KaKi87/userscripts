// ==UserScript==
// @match       https://search.brave.com/search
// @name        Cloudflare detector for Brave Search
// @description Tag Brave Search results pointing to Cloudflare-protected websites
// @grant       GM.xmlHttpRequest
// @version     0.1.2
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/braveSearchCloudflareDetector
// @require     https://cdn.jsdelivr.net/npm/@trim21/gm-fetch
// @require     https://git.kaki87.net/KaKi87/userscripts/raw/commit/9233234ec6bffeaa2ab663e4f4144a3615bf93a6/_lib/isCloudflareDomain.js
// ==/UserScript==

(async () => {
    for(const resultElement of document.querySelectorAll('.snippet, .card-body')){
        const domain =
            resultElement.querySelector('.netloc')?.textContent
            ||
            resultElement.querySelector('.card-source')?.childNodes[2].textContent.trim();
        if(domain && await isCloudflareDomain(domain, GM_fetch)){
            const cloudflareIndicatorElement = document.createElement('span');
            cloudflareIndicatorElement.textContent = 'Cloudflare';
            cloudflareIndicatorElement.setAttribute(
                'style',
                [
                    'padding: 0 0.25rem',
                    'background-color: #C0392B',
                    'color: #ECF0F1',
                    'font-size: 12px',
                    'border-radius: 3px'
                ].join(';')
            );
            resultElement.insertAdjacentElement('afterbegin', cloudflareIndicatorElement);
        }
    }
})();
[← Homepage](../README.md)

# Enhancements for Forgejo

| [Install](../../../../raw/branch/master/enhancementsForForgejo/main.user.js) |
|------------------------------------------------------------------------------|

## Features

- ~~Fix alphabetical files ordering~~ ([`forgejo/forgejo@codeberg.org#317`](https://codeberg.org/forgejo/forgejo/issues/317), fixed by [`forgejo/forgejo@codeberg.org#2522`](https://codeberg.org/forgejo/forgejo/pulls/2522))
- Fix comment buttons alignment ([`forgejo/forgejo@codeberg.org#560`](https://codeberg.org/forgejo/forgejo/issues/560))
- Sticky navbar ([`forgejo/forgejo@codeberg.org#1318`](https://codeberg.org/forgejo/forgejo/issues/1318))
- Scrollable code view
- Fix issues tab consistency ([`forgejo/forgejo@codeberg.org#365`](https://codeberg.org/forgejo/forgejo/issues/365))
- Restore feature-rich Markdown editor ([`forgejo/forgejo@codeberg.org#3604`](https://codeberg.org/forgejo/forgejo/issues/3604))
- Sort `/admin/users` by newest by default ([`forgejo/forgejo@codeberg.org#2574`](https://codeberg.org/forgejo/forgejo/issues/2574))

---

| ![](./screenshot1.png)  | ![](./screenshot2.png) |
|-------------------------|------------------------|
| ![](./screenshot3.webp) |                        |
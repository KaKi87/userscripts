[← Homepage](../README.md)

# JSBin Enhanced

Dark mode & GUI settings for JSBin.

| [Install](../../../../raw/branch/master/jsbinEnhanced/main.user.js) |
|---------------------------------------------------------------------|

| ![](./screenshot1.png) | ![](./screenshot2.png) |
|------------------------|------------------------|
| ![](./screenshot3.png) | ![](./screenshot4.png) |
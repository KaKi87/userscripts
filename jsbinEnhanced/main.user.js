// ==UserScript==
// @match       https://jsbin.com/*
// @name        JSBin Enhanced
// @description Dark mode & GUI settings
// @grant       GM.getValue
// @grant       GM.setValue
// @version     0.1.1
// @author      KaKi87
// @license     WTFPL
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/jsbinEnhanced
// @run-at      document-start
// @require     https://git.kaki87.net/KaKi87/userscripts/raw/commit/1f4fdf13ce6d48a89c2e4fd8217aa5f4043c68a8/_lib/createConsoleInterceptor.js
// @require     https://cdn.jsdelivr.net/npm/vue@3.2.37/dist/vue.global.prod.js
// ==/UserScript==

unsafeWindow.Vue = Vue;

const
    log = window.console.log.bind(console),
    createMenu = ({
        title,
        items
    }) => {
        const
            menuElement = document.createElement('div'),
            menuTitleElement = document.createElement('a'),
            menuDropdownElement = document.createElement('div'),
            menuDropdownChildElement = document.createElement('div');
        menuElement.classList.add('menu');
        menuElement.addEventListener(
            'click',
            event => {
                event.preventDefault();
                menuElement.classList.toggle('open');
            }
        );
        menuElement.appendChild(menuTitleElement);
        menuElement.appendChild(menuDropdownElement);
        menuTitleElement.classList.add('button', 'group', 'button-dropdown', 'button-dropdown-arrow');
        menuTitleElement.setAttribute('href', '#');
        menuTitleElement.textContent = title;
        menuDropdownElement.classList.add('dropdown');
        menuDropdownElement.appendChild(menuDropdownChildElement);
        menuDropdownChildElement.classList.add('dropdownmenu');
        for(const {
            title,
            onClick
        } of items){
            const menuDropdownChildItemElement = document.createElement('a');
            menuDropdownChildItemElement.classList.add('button', 'group');
            menuDropdownChildItemElement.textContent = title;
            menuDropdownChildItemElement.addEventListener(
                'click',
                async event => {
                    event.preventDefault();
                    await new Promise(resolve => setTimeout(resolve, 0));
                    menuElement.classList.remove('open');
                    onClick();
                }
            );
            menuDropdownChildElement.appendChild(menuDropdownChildItemElement);
        }
        return menuElement;
    },
    createModal = options => {
        const modalElement = document.createElement('div');
        modalElement.classList.add('__JSBinEnhanced__modal');
        const app = Vue.createApp({
            components: {
                modal: {
                    props: {
                        title: undefined
                    },
                    methods: {
                        close: function(){
                            this.$emit('close');
                        }
                    },
                    template: `
                        <div
                            class="__JSBinEnhanced__modal__backdrop"
                            @click="close"
                        ></div>
                        <div class="__JSBinEnhanced__modal__content">
                            <header class="__JSBinEnhanced__modal__content__header">
                                <h1>{{ title }}</h1>
                                <a
                                    class="__JSBinEnhanced__modal__content__header__close"
                                    href="#"
                                    role="button"
                                    @click.prevent="close"
                                ></a>
                            </header>
                            <main class="__JSBinEnhanced__modal__content__main">
                                <slot name="main"></slot>
                            </main>
                            <footer class="__JSBinEnhanced__modal__content__footer">
                                <slot name="footer"></slot>
                            </footer>
                        </div>
                    `
                }
            },
            ...options({
                unmount: () => app.unmount()
            })
        });
        app.mount(modalElement);
        return modalElement;
    },
    createPreferencesModal = ({
        onClose
    }) => createModal(({
        unmount
    }) => ({
        data: () => ({
            settings: JSON.parse(JSON.stringify(unsafeWindow.jsbin.settings)),
            isSaved: true
        }),
        methods: {
            close: function(){
                if(!this.isSaved){
                    if(!window.confirm('Discard changes?'))
                        return;
                }
                unmount();
                onClose();
            },
            refreshSaved: function(){
                this.isSaved = JSON.stringify(unsafeWindow.jsbin.settings) === JSON.stringify(this.settings);
            },
            save: function(){
                Object.assign(unsafeWindow.jsbin.settings, JSON.parse(JSON.stringify(this.settings)));
                this.refreshSaved();
            },
            reload: function(){
                window.location.reload();
            },
            saveAndReload: function(){
                this.save();
                this.reload();
            }
        },
        watch: {
            settings: {
                deep: true,
                handler: function(){
                    this.refreshSaved();
                }
            }
        },
        template: `
            <modal
                title="Preferences"
                @close="close"
            >
                <template v-slot:main>
                    <table class="__JSBinEnhanced__modal__content__main__items">
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.editor.theme">Theme</label></td>
                            <td><input id="settings.editor.theme" type="text" v-model="settings.editor.theme"></td>
                        </tr>
                        <tr>
                            <td><label for="settings.font">Font size</label></td>
                            <td><input id="settings.font" type="number" v-model="settings.font"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.editor.tabSize">Tab size</label></td>
                            <td><input id="settings.editor.tabSize" type="number" v-model="settings.editor.tabSize"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.editor.indentWithTabs">Space indent</label></td>
                            <td><input id="settings.editor.indentWithTabs" type="checkbox" v-model="settings.editor.indentWithTabs" :true-value="false" :false-value="true"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.editor.indentUnit">Space indent size</label></td>
                            <td><input id="settings.editor.indentUnit" type="number" v-model="settings.editor.indentUnit" :disabled="settings.editor.indentWithTabs"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.editor.lineNumbers">Line numbers</label></td>
                            <td><input id="settings.editor.lineNumbers" type="checkbox" v-model="settings.editor.lineNumbers"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.editor.lineWrapping">Line wrapping</label></td>
                            <td><input id="settings.editor.lineWrapping" type="checkbox" v-model="settings.editor.lineWrapping"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.addons.activeline">Active line highlighting</label></td>
                            <td><input id="settings.addons.activeline" type="checkbox" v-model="settings.addons.activeline"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.addons.fold">Folding</label></td>
                            <td><input id="settings.addons.fold" type="checkbox" v-model="settings.addons.fold"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.addons.closebrackets">Bracket auto-closing</label></td>
                            <td><input id="settings.addons.closebrackets" type="checkbox" v-model="settings.addons.closebrackets"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.editor.matchBrackets">Bracket matching</label></td>
                            <td><input id="settings.editor.matchBrackets" type="checkbox" v-model="settings.editor.matchBrackets"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.addons.highlight">Keyword matching</label></td>
                            <td><input id="settings.addons.highlight" type="checkbox" v-model="settings.addons.highlight"></td>
                        </tr>
                        <tr class="__JSBinEnhanced__modal__content__main__items__item--requires-reload">
                            <td><label for="settings.debug">Debug mode</label></td>
                            <td><input id="settings.debug" type="checkbox" v-model="settings.debug"></td>
                        </tr>
                    </table>
                </template>
                <template v-slot:footer>
                    <p>* Reloading is required to apply this change.</p>
                    <button @click="close">{{ isSaved ? 'Exit' : 'Cancel' }}</button>
                    <button
                        @click="save"
                        :disabled="isSaved"
                    >Save</button>
                    <button
                        @click="reload"
                        :disabled="!isSaved"
                    >Reload</button>
                    <button
                        @click="saveAndReload"
                        :disabled="isSaved"
                    >Save & reload</button>
                </template>
            </modal>
        `
    })),
    createTweaksModal = ({
        tweaks,
        saveTweaks,
        onClose
    }) => createModal(({
        unmount
    }) => ({
        data: () => ({
            tweaksJson: JSON.stringify(tweaks),
            tweaks
        }),
        computed: {
            isSaved: function(){
                return this.tweaksJson === JSON.stringify(this.tweaks);
            }
        },
        methods: {
            close: function(){
                if(!this.isSaved){
                    if(!window.confirm('Discard changes?'))
                        return;
                }
                unmount();
                onClose();
            },
            save: async function(){
                await saveTweaks(this.tweaks);
                this.tweaksJson = JSON.stringify(this.tweaks);
            }
        },
        template: `
            <modal
                title="Tweaks"
                @close="close"
            >
                <template v-slot:main>
                    <table class="__JSBinEnhanced__modal__content__main__items">
                        <tr>
                            <td><label for="tweaks.isDark">Dark mode</label></td>
                            <td><input id="tweaks.isDark" type="checkbox" v-model="tweaks.isDark"></td>
                        </tr>
                        <tr>
                            <td><label for="tweaks.editorFontFamily">Editor font family</label></td>
                            <td><input id="tweaks.editorFontFamily" type="text" v-model="tweaks.editorFontFamily"></td>
                        </tr>
                        <tr>
                            <td><label for="tweaks.isInfoCardHidden">Hide info card</label></td>
                            <td><input id="tweaks.isInfoCardHidden" type="checkbox" v-model="tweaks.isInfoCardHidden"></td>
                        </tr>
                        <tr>
                            <td><label for="tweaks.isAdHidden">Hide advertisement</label></td>
                            <td><input id="tweaks.isAdHidden" type="checkbox" v-model="tweaks.isAdHidden"></td>
                        </tr>
                    </table>
                </template>
                <template v-slot:footer>
                    <button @click="close">{{ isSaved ? 'Exit' : 'Cancel' }}</button>
                    <button
                        @click="save"
                        :disabled="isSaved"
                    >Save</button>
                </template>
            </modal>
        `
    })),
    applyTweaks = ({
        isDark,
        isInfoCardHidden,
        isAdHidden,
        editorFontFamily
    }) => {
        if(typeof isDark === 'boolean')
            document.body.classList.toggle('__JSBinEnhanced__dark', isDark);
        if(typeof isInfoCardHidden === 'boolean')
            document.body.classList.toggle('__JSBinEnhanced__hiddenInfoCard', isInfoCardHidden);
        if(typeof isAdHidden === 'boolean')
            document.body.classList.toggle('__JSBinEnhanced__isAdHidden', isAdHidden);
        document.body.classList.toggle('__JSBinEnhanced__customEditorFontFamily', !!editorFontFamily);
        document.body.style.setProperty('--JSBinEnhanced--editor-font-family', editorFontFamily || '');
    };
window.console.log = createConsoleInterceptor(
    {
        log
    },
    {
        log: async args => {
            if(args[0].startsWith('To edit settings, type this string into the console: '))
                unsafeWindow[args[0].slice(53)];
            if(args[0] === 'jsbin.settings can now be modified on the console'){
                document
                    .querySelector('#control .buttons')
                    .appendChild(
                        createMenu({
                            title: 'Settings',
                            items: [
                                {
                                    title: 'Preferences',
                                    onClick: () => {
                                        const preferencesModalElement = createPreferencesModal({
                                            onClose: () => document.body.removeChild(preferencesModalElement)
                                        });
                                        document.body.appendChild(preferencesModalElement);
                                    }
                                },
                                {
                                    title: 'Tweaks',
                                    onClick: async () => {
                                        const tweaksModalElement = createTweaksModal({
                                            tweaks: JSON.parse(await GM.getValue('tweaks') || '{}'),
                                            saveTweaks: async tweaks => {
                                                await GM.setValue('tweaks', JSON.stringify(tweaks));
                                                applyTweaks(tweaks);
                                            },
                                            onClose: () => document.body.removeChild(tweaksModalElement)
                                        });
                                        document.body.appendChild(tweaksModalElement);
                                    }
                                },
                                {
                                    title: 'About',
                                    onClick: () => {
                                        window.open('https://git.kaki87.net/KaKi87/userscripts/src/branch/master/jsbinEnhanced');
                                    }
                                }
                            ]
                        })
                    );
                applyTweaks(JSON.parse(await GM.getValue('tweaks') || '{}'));
            }
        }
    }
).log;

{
    const styleElement = document.createElement('style');
    // language=CSS
    styleElement.appendChild(document.createTextNode(`
        #control .buttons .menu.hidden {
            top: inherit;
            opacity: 0.5;
            cursor: not-allowed;
        }
        #control .buttons .menu.hidden .button {
            pointer-events: none;
        }

        body.__JSBinEnhanced__dark {
            color-scheme: dark;
            background-color: black;
        }
        body.__JSBinEnhanced__dark #toppanel,
        body.__JSBinEnhanced__dark #control,
        body.__JSBinEnhanced__dark #console {
            filter: invert(1) hue-rotate(180deg) contrast(.7) !important;
            background: #FFF;
        }
        body.__JSBinEnhanced__hiddenInfoCard #infocard {
            display: none;
        }
        body.__JSBinEnhanced__isAdHidden main {
            height: 100%;
        }
        body.__JSBinEnhanced__isAdHidden #ea-wrapper {
            display: none;
        }
        body.__JSBinEnhanced__customEditorFontFamily .CodeMirror * {
            font-family: var(--JSBinEnhanced--editor-font-family);
        }

        .__JSBinEnhanced__modal * { box-sizing: border-box; }
        .__JSBinEnhanced__modal h1 { font-size: inherit; }
        .__JSBinEnhanced__modal a { color: inherit; text-decoration: none; }
        .__JSBinEnhanced__modal input:not([type=checkbox]) { width: 100%; }

        .__JSBinEnhanced__modal {
            --JSBinEnhanced--color-foreground: #232323;
            --JSBinEnhanced--color-background: white;
            --JSBinEnhanced--border: 1px solid var(--JSBinEnhanced--color-foreground);
        }
        body.__JSBinEnhanced__dark .__JSBinEnhanced__modal {
            --JSBinEnhanced--color-foreground: white;
            --JSBinEnhanced--color-background: #232323;
        }

        .__JSBinEnhanced__modal,
        .__JSBinEnhanced__modal__backdrop {
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
        }
        .__JSBinEnhanced__modal {
            z-index: 100000;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .__JSBinEnhanced__modal__backdrop {
            z-index: -1;
            background-color: rgba(0, 0, 0, 0.25);
        }
        .__JSBinEnhanced__modal__content {
            color: var(--JSBinEnhanced--color-foreground);
            background-color: var(--JSBinEnhanced--color-background);
            border: var(--JSBinEnhanced--border);
            box-shadow: rgba(0, 0, 0, 0.35) 0 5px 15px;
            width: 75vh;
            height: 75vh;
            display: flex;
            flex-direction: column;
        }
        .__JSBinEnhanced__modal__content__header,
        .__JSBinEnhanced__modal__content__main,
        .__JSBinEnhanced__modal__content__footer {
            padding: 1rem;
        }
        .__JSBinEnhanced__modal__content__header,
        .__JSBinEnhanced__modal__content__footer {
            padding: 1rem;
            display: flex;
            align-items: center;
        }
        .__JSBinEnhanced__modal__content__header {
            border-bottom: var(--JSBinEnhanced--border);
        }
        .__JSBinEnhanced__modal__content__header__close {
            margin-left: auto;
            background-color: var(--JSBinEnhanced--color-foreground);
            border-radius: 50%;
            width: 1.5rem;
            height: 1.5rem;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .__JSBinEnhanced__modal__content__header__close:after {
            content: '✖';
            font-size: 0.75rem;
            color: var(--JSBinEnhanced--color-background);
        }
        .__JSBinEnhanced__modal__content__main {
            flex: 1;
        }
        .__JSBinEnhanced__modal__content__main__items {
            width: 100%;
            border-spacing: 0.5rem;
        }
        .__JSBinEnhanced__modal__content__main__items td:not(:last-child) {
            width: 0;
            white-space: nowrap;
        }
        .__JSBinEnhanced__modal__content__main__items__item--requires-reload td:first-child:after {
            content: '*';
        }
        .__JSBinEnhanced__modal__content__footer {
            border-top: var(--JSBinEnhanced--border);
            column-gap: 0.5rem;
        }
        .__JSBinEnhanced__modal__content__footer button:first-of-type {
            margin-left: auto;
        }
    `));
    document.head.appendChild(styleElement);
}
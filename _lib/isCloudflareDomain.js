const
    cloudflareCidrList = [
        // Source : https://www.cloudflare.com/ips-v4
        '173.245.48.0/20',
        '103.21.244.0/22',
        '103.22.200.0/22',
        '103.31.4.0/22',
        '141.101.64.0/18',
        '108.162.192.0/18',
        '190.93.240.0/20',
        '188.114.96.0/20',
        '197.234.240.0/22',
        '198.41.128.0/17',
        '162.158.0.0/15',
        '104.16.0.0/12',
        '172.64.0.0/13',
        '131.0.72.0/22',
    ],
    isIpInCidr = (ip, cidr) => {
        // Source : https://gist.github.com/KaKi87/7c3907a1ec03ebc8ecb0294ab7176bce
        const
            [range, bits = 32] = cidr.split('/'),
            mask = ~(2 ** (32 - bits) - 1),
            ip4ToInt = ip => ip.split('.').reduce((int, oct) => (int << 8) + parseInt(oct, 10), 0) >>> 0;
        return (ip4ToInt(ip) & mask) === (ip4ToInt(range) & mask);
    };

var isCloudflareDomain = async (domain, fetch = window.fetch) => {
    const { success, data } = await (await fetch(`https://api.kaki87.net/dns/${domain}?record=A`)).json();
    if(!success) return;
    const ip = data.find(item => item.type === 'A')?.value;
    if(!ip) return;
    return cloudflareCidrList.some(cidr => isIpInCidr(ip, cidr));
};
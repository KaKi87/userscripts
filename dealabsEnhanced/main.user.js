// ==UserScript==
// @match       https://www.dealabs.com/*
// @name        Enhancements for Dealabs
// @description Filtering enhancements for Dealabs.com
// @grant       none
// @version     0.1.1
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/dealabsEnhanced
// ==/UserScript==

const
    APP_ID = 'dealabsEnhanced',
    settingsElement = document.createElement('div'),
    items = [...document.querySelectorAll('[id^="thread_"]')].map((element, index) => {
        const isExpired = !!element.querySelector('.cept-show-expired-threads');
        if(isExpired)
            element.classList.add(`__${APP_ID}__item--expired`);
        return {
            isExpired,
            price: parseFloat(element.querySelector('.thread-price').textContent.replace(',', '.')),
            element,
            index
        };
    }),
    itemsContainerElement = items[0].element.parentElement;
settingsElement.classList.add(`__${APP_ID}__filters`, 'listLayout');
{
    const
        sortLabelElement = document.createElement('label'),
        sortTextElement = document.createElement('span'),
        sortSelectElement = document.createElement('select');
    sortLabelElement.classList.add(`__${APP_ID}__filters__filter`);
    sortTextElement.textContent = 'Trier par : ';
    sortLabelElement.appendChild(sortTextElement);
    sortSelectElement.innerHTML = `
        <option value="default" selected>Pertinence</option>
        <option value="price-asc">Prix croissant</option>
        <option value="price-desc">Prix décroissant</option>
    `;
    sortSelectElement.addEventListener('input', () => {
        switch(sortSelectElement.value){
            case 'default': {
                items.sort((a, b) => a.index - b.index);
                break;
            }
            case 'price-asc': {
                items.sort((a, b) => a.price - b.price);
                break;
            }
            case 'price-desc': {
                items.sort((a, b) => b.price - a.price);
                break;
            }
        }
        itemsContainerElement.replaceChildren(...items.map(({ element }) => element));
    });
    sortLabelElement.appendChild(sortSelectElement);
    settingsElement.appendChild(sortLabelElement);
}
{
    const
        expiredLabelElement = document.createElement('label'),
        expiredTextElement = document.createElement('span'),
        expiredCheckboxElement = document.createElement('input'),
        expiredStyles = new CSSStyleSheet();
    expiredLabelElement.classList.add(`__${APP_ID}__filters__filter`);
    expiredTextElement.textContent = 'Éléments expirés';
    expiredLabelElement.appendChild(expiredTextElement);
    expiredCheckboxElement.setAttribute('type', 'checkbox');
    expiredCheckboxElement.setAttribute('checked', '');
    expiredCheckboxElement.addEventListener('input', () => {
        expiredStyles.replace(
            expiredCheckboxElement.checked
                ? ''
                : `
                    .__${APP_ID}__item--expired {
                        display: none;
                    }
                `
        );
    });
    expiredLabelElement.appendChild(expiredCheckboxElement);
    settingsElement.appendChild(expiredLabelElement);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, expiredStyles];
}
{
    const
        sidebarLabelElement = document.createElement('label'),
        sidebarTextElement = document.createElement('span'),
        sidebarCheckboxElement = document.createElement('input'),
        sidebarStyles = new CSSStyleSheet();
    sidebarLabelElement.classList.add(`__${APP_ID}__filters__filter`);
    sidebarTextElement.textContent = 'Barre latérale';
    sidebarLabelElement.appendChild(sidebarTextElement);
    sidebarCheckboxElement.setAttribute('type', 'checkbox');
    sidebarCheckboxElement.setAttribute('checked', '');
    sidebarCheckboxElement.addEventListener('input', () => {
        sidebarStyles.replace(
            sidebarCheckboxElement.checked
                ? ''
                : `
                    .listLayout-side {
                        display: none;
                    }
                    .cept-event-deals {
                        width: 100%;
                    }
                `
        );
    });
    sidebarLabelElement.appendChild(sidebarCheckboxElement);
    settingsElement.appendChild(sidebarLabelElement);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, sidebarStyles];
}
document.querySelector('#content-list').insertAdjacentElement('beforebegin', settingsElement);
{
    const styles = new CSSStyleSheet();
    styles.replace(`
        .__${APP_ID}__filters {
            padding: 1rem;
            display: flex;
            align-items: center;
            column-gap: 1rem;
            overflow-x: auto;
        }
        .__${APP_ID}__filters__filter {
            display: flex;
            align-items: center;
            column-gap: 0.5rem;
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
}
// ==UserScript==
// @match         https://www.google.*/search*
// @name          Untranslate Reddit for Google Search
// @description   Untranslate Reddit results from Google Search
// @grant         GM.xmlHttpRequest
// @version       0.2.0
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/untranslateRedditForGoogleSearch
// ==/UserScript==

(async () => {
    for(const element of document.querySelectorAll('a[href*="reddit.com"][href*="/?tl="]')){
        const titleElement = element.querySelector('[role="link"], h3');
        if(!titleElement) continue;
        const url = new URL(element.getAttribute('href'));
        url.hostname = 'old.reddit.com';
        // noinspection JSVoidFunctionReturnValueUsed,JSCheckFunctionSignatures
        const
            { response: document } = await GM.xmlHttpRequest({ url, responseType: 'document' }),
            og = [...document.querySelectorAll('[property^="og:"]')].reduce((og, element) => ({
                ...og,
                [element.getAttribute('property').slice(3)]:
                    element.getAttribute('content')
            }), {});
        titleElement.textContent = og.title;
    }
})().catch(console.error);
[← Homepage](../README.md)

# AlternativeTo free/freemium differentiator

Allow searching *free* while excluding *freemium* apps on [alternativeto.net](https://alternativeto.net/).

| [Install](../../../../raw/branch/master/AlternativeToFreeFreemiumDifferentiator/main.user.js) |
|-----------------------------------------------------------------------------------------------|

## DEPRECATION NOTICE

This project is now deprecated in favor of [AlternativeTo Enhanced](../alternativetoEnhanced/README.md).

---

**Works on the legacy AlternativeTo version :**

![](./screenshot1.png)

**Works on the React.JS AlternativeTo version :**

![](./screenshot2.png)

What this userscript actually does :
1. it renames the `Free` filter to `Free or freemium`, as it actually acts that way,
2. it adds an actual `Free` filter that uses the same search results while excluding `Freemium`.

Note that it doesn't provide a `Freemium` only filter, as it would be counterproductive regarding its initial objective.

## Changelog

* `1.0.0` (2020-11-24) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/b7ceddf83e9360489c66094c613208b4d96f1862/AlternativeToFreeFreemiumDifferentiator)
* `1.0.1` (2020-12-13) • [Fix init mutation observer sometimes not executing](https://git.kaki87.net/KaKi87/userscripts/src/commit/8f0fc20b3f56f867f34aaaa99376c07e9b113a75/AlternativeToFreeFreemiumDifferentiator)
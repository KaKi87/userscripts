// ==UserScript==
// @match       https://alternativeto.net/software/*
// @name        AlternativeTo.net free/freemium differentiator
// @description Differenciate free and freemium in alternative apps list
// @grant       none
// @version     1.0.1
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/AlternativeToFreeFreemiumdifferentiator
// ==/UserScript==

const
    query = () => new URLSearchParams(window.location.href.split('?')[1]),
    isExcludingFreemiumApps = query().has('exclude-freemium-apps');

if(document.querySelector('#__next')){
    const init = () => {
        const
            freeOrFreemiumLicenseFilter = document.querySelector('.license-free:not(.exclude-freemium-apps)'),
            openSourceLicenseFilter = document.querySelector('.license-open'),
            paidLicenseFilter = document.querySelector('.license-paid'),
            freeLicenseFilter =  document.querySelector('.exclude-freemium-apps') || freeOrFreemiumLicenseFilter.cloneNode(true),
            _query = query();

        if(!freeLicenseFilter.classList.contains('exclude-freemium-apps')){
            freeLicenseFilter.classList.add('exclude-freemium-apps');
            freeLicenseFilter.addEventListener('click', () => {
                _query.set('license', 'free');
                _query.set('exclude-freemium-apps', '');
                window.location.replace(`${window.location.href.split('?')[0]}?${_query.toString()}`);
            });

            freeOrFreemiumLicenseFilter.children[0].children[0].textContent = 'Free or freemium';
            freeOrFreemiumLicenseFilter.parentElement.insertBefore(freeLicenseFilter, freeOrFreemiumLicenseFilter);
        }

        if(isExcludingFreemiumApps){
            const switchLicenseFilter = license => {
                _query.set('license', license);
                _query.delete('exclude-freemium-apps');
                window.location.replace(`${window.location.href.split('?')[0]}?${_query.toString()}`);
            };
            freeOrFreemiumLicenseFilter.addEventListener('click', () => switchLicenseFilter('free'));
            openSourceLicenseFilter.addEventListener('click', () => switchLicenseFilter('opensource'));
            paidLicenseFilter.addEventListener('click', () => switchLicenseFilter('paid'));
            document.querySelectorAll('.freemium')
                    .forEach(el => el.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.style.display = 'none');
        }

        freeOrFreemiumLicenseFilter.classList.toggle('current', !isExcludingFreemiumApps && _query.get('license') === 'free');
        freeLicenseFilter.classList.toggle('current', isExcludingFreemiumApps);
    };

    (new MutationObserver(() => {
        if(!document.querySelector('html').classList.contains('nprogress-busy'))
            init();
    })).observe(document.querySelector('html'), { attributes: true });

    (new MutationObserver(init))
        .observe(document.querySelector('.filter-bar'), { childList: true });
}
else {
    const init = () => {
        const
            freeOrFreemiumLicenseFilter = document.querySelector('[data-filter="free"]').parentElement,
            freeLicenseFilter = freeOrFreemiumLicenseFilter.cloneNode(true);

        freeLicenseFilter.children[0].href += '&exclude-freemium-apps';

        freeOrFreemiumLicenseFilter.children[0].textContent = 'Free or freemium';
        freeOrFreemiumLicenseFilter.parentElement.insertBefore(freeLicenseFilter, freeOrFreemiumLicenseFilter);

        if(isExcludingFreemiumApps)
            document.querySelectorAll('.jq_filterbarPanels .main-panel a')
                .forEach(el => el.href += '&exclude-freemium-apps');
    };
    init();
    (new MutationObserver(init)).observe(document.querySelector('.jq_filterbarPanels'), { childList: true });

    const activeLicenseFilter = document.querySelector('.license-filter.has-filter');
    if(activeLicenseFilter && activeLicenseFilter.childNodes[0].textContent === 'Free'){
        if(isExcludingFreemiumApps){
            const removeFreemiumApps = () => [
                    ...([...document.querySelectorAll('#alternativeList .pricing-free')]
                        .filter(el => el.textContent === 'Freemium')),
                    ...document.querySelectorAll('#alternativeList .price-indicator-filled')
                ]
                .forEach(el => {
                    const appItem = el.parentElement.parentElement.parentElement.parentElement.parentElement;
                    if(appItem.parentElement)
                        appItem.parentElement.removeChild(appItem);
                });
            removeFreemiumApps();
            (new MutationObserver(removeFreemiumApps))
                .observe(document.querySelector('#alternativeList'), { childList: true });
        }
        else
            activeLicenseFilter.childNodes[0].textContent = 'Free or freemium';
    }
}
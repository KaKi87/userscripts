// ==UserScript==
// @match       https://www.google.*/search*
// @name        Cloudflare detector for Google Search
// @description Tag Google Search results pointing to Cloudflare-protected websites
// @grant       none
// @version     2.0.1
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/GoogleSearchCloudFlareDetector
// @require     https://git.kaki87.net/KaKi87/userscripts/raw/commit/9233234ec6bffeaa2ab663e4f4144a3615bf93a6/_lib/isCloudflareDomain.js
// ==/UserScript==

const
    selectors = [
        { resultElementSelector: 'a:has(h3)', cloudflareIndicatorParentElement: resultElement => resultElement.parentElement.parentElement.parentElement.parentElement.parentElement },
        { resultElementSelector: 'a:has(div[role="heading"])', cloudflareIndicatorParentElement: resultElement => resultElement },
        { resultElementSelector: 'h3 a', cloudflareIndicatorParentElement: resultElement => resultElement.parentElement.parentElement.parentElement }
    ],
    resultElements = [];
new MutationObserver(async () => {
    for(const { resultElementSelector, cloudflareIndicatorParentElement } of selectors){
        for(const resultElement of document.querySelectorAll(resultElementSelector)){
            if(!resultElements.includes(resultElement)){
                resultElements.push(resultElement);
                const domain = new URL(resultElement.getAttribute('href')).host;
                if(await isCloudflareDomain(domain)){
                    const cloudflareIndicatorElement = document.createElement('span');
                    cloudflareIndicatorElement.textContent = 'Cloudflare';
                    cloudflareIndicatorElement.setAttribute(
                        'style',
                        [
                            'align-self: flex-start',
                            'padding: 0 0.25rem',
                            'background-color: #C0392B',
                            'color: #ECF0F1',
                            'font-size: 12px',
                            'border-radius: 3px'
                        ].join(';')
                    );
                    cloudflareIndicatorParentElement(resultElement).insertAdjacentElement('afterbegin', cloudflareIndicatorElement);
                }
            }
        }
    }
}).observe(document.body, { childList: true, subtree: true });
[← Homepage](../README.md)

# Cloudflare detector for Google search *[Why?](https://framagit.org/dCF/deCloudflare/-/blob/master/readme/en.md)*

Tag Google search results pointing to Cloudflare-protected websites.

| [Install](../../../../raw/branch/master/GoogleSearchCloudFlareDetector/main.user.js) |
|--------------------------------------------------------------------------------------|

![](./screenshot.jpg)

## Changelog

* `1.0.0` (2020-02-20) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/3330f9a72505d8d9b128ca6c990b20cc1848f524/GoogleSearchCloudFlareDetector)
* `1.1.0` (2020-12-30) • [Add mobile support](https://git.kaki87.net/KaKi87/userscripts/src/commit/12ab1df34f9ab6b7df60131cc779b6cdbc032121/GoogleSearchCloudFlareDetector)
* `1.1.1` (2022-12-10) • [Fix HTML injection](https://git.kaki87.net/KaKi87/userscripts/src/commit/b7d725fb149514906f7045a3b27adf28869b0d09/GoogleSearchCloudFlareDetector)
* `1.1.2` (2023-01-03) • [Fix support for KaKi87/deno-api@e118d4d](https://git.kaki87.net/KaKi87/userscripts/src/commit/9233234ec6bffeaa2ab663e4f4144a3615bf93a6/GoogleSearchCloudFlareDetector)
* `1.1.3` (2023-01-04) • [Fix 9233234](https://git.kaki87.net/KaKi87/userscripts/src/commit/0a179fb0eb3d4f10ab0de79ba0cba9484ddd46bb/GoogleSearchCloudFlareDetector)
* `2.0.0` (2023-04-22) • [Rewrite](https://git.kaki87.net/KaKi87/userscripts/src/commit/3ac2da59e26a40b72d08f430f3a31dea7b44fb7e)
* `2.0.1` (2024-03-01) • Fix tag position
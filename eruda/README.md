[← Homepage](../README.md)

# Eruda

Dynamically load [Eruda DevTools](https://github.com/liriliri/eruda) on any website by shaking your device.

Usable on any browser with [AdGuard Adblocker](https://adguard.com/en/adguard-android/overview.html) or on [Bromite Browser](https://github.com/bromite/bromite).

| [Install](../../../../raw/branch/master/eruda/main.user.js) |
|-------------------------------------------------------------|
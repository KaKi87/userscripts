// ==UserScript==
// @match         *://*/*
// @name          Eruda
// @description   Shake to enable Eruda devtools
// @grant         GM.addElement
// @version       0.1.1
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/eruda
// @require       https://unyt.land/gh/izeryab/ShakeTS/Shake.ts
// ==/UserScript==

(new Shake()).start();
window.addEventListener(
    'shake',
    async () => {
        if(!confirm('Open DevTools?')) return;
        const erudaScriptElement = (GM.addElement || document.createElement).call(document, 'script');
        erudaScriptElement.setAttribute('src', 'https://cdn.jsdelivr.net/npm/eruda');
        if(!GM.addElement)
            document.head.appendChild(erudaScriptElement);
        await new Promise(resolve => erudaScriptElement.addEventListener('load', resolve));
        eruda.init();
        eruda.show();
        eruda.show('elements');
    }
);
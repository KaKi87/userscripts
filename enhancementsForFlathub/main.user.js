// ==UserScript==
// @match         https://flathub.org/apps/*
// @name          Enhancements for Flathub
// @description   Add `-y` parameter to Flathub `flatpak install` commands
// @grant         none
// @version       0.1.0
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/enhancementsForFlathub
// @require       https://git.kaki87.net/KaKi87/userscripts/raw/commit/0da5a1c5d535fb40588d1c421170fe79790e7724/_lib/waitForSelector.js
// ==/UserScript==

const getElementByXpath = path =>
    document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE).singleNodeValue;

(async () => {
    (await waitForSelector('[id="headlessui-popover-button-:R1bjmqd6:"]'))
        .addEventListener('click', async () => {
            await new Promise(resolve => setTimeout(resolve, 0));
            const commandElement = getElementByXpath(`//*[starts-with(text(), 'flatpak install flathub ')]`);
            commandElement.childNodes[0].textContent += ' -y';
            commandElement.childNodes[1].addEventListener(
                'click',
                async () => await
                    window.navigator.clipboard.writeText(
                        commandElement.childNodes[0].textContent
                    )
            );
        });
})().catch(console.error);
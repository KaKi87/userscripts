[← Homepage](../README.md)

# Enhancements for Flathub

Add `-y` parameter to Flathub `flatpak install` commands

| [Install](../../../../raw/branch/master/enhancementsForFlathub/main.user.js) |
|------------------------------------------------------------------------------|

![](./screenshot.webp)

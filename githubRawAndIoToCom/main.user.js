// ==UserScript==
// @match       https://raw.githubusercontent.com/*
// @match       https://*.github.io/*
// @name        GitHub RAW & IO to COM
// @description Redirects raw.githubusercontent.com & github.io to github.com
// @grant       GM.registerMenuCommand
// @version     0.2.0
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/githubRawAndIoToCom
// ==/UserScript==

const
    addMenuItem = ({
        username,
        repo,
        branch,
        path
    }) => {
        const
            shortUrl = `github.com/${username}/${repo}`,
            fullUrl = `https://${shortUrl}${path ? `/blob/${branch}/${path}` : ''}`;
        GM.registerMenuCommand(
            `Open ${shortUrl}`,
            () => window.location.assign(fullUrl)
        );
    },
    [
        subdomain,
        domain
    ] = window.location.host.split('.');
if(domain === 'githubusercontent'){
    // Raw
    const [
        , username
        , repo
        , branch
        , ...path
    ] = window.location.pathname.split('/');
    addMenuItem({
        username,
        repo,
        branch,
        path: path.join('/')
    });
}
else if(domain === 'github'){
    // Domain is repo
    addMenuItem({
        username: subdomain,
        repo: window.location.hostname,
        branch: '-',
        path: window.location.pathname.slice(1)
    });
    // Top directory is repo
    {
        const [
            , repo
            , ...path
        ] = window.location.pathname.split('/');
        if(repo) addMenuItem({
            username: subdomain,
            repo,
            branch: '-',
            path: path.join('/')
        });
    }
}
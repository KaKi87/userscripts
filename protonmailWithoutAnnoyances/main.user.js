// ==UserScript==
// @match       https://*.proton.me/*
// @name        ProtonMail without annoyances
// @description Remove annoying ProtonMail buttons & banners
// @grant       GM.addStyle
// @version     0.1.4
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/protonmailWithoutAnnoyances
// ==/UserScript==

{
    // language=CSS
    GM.addStyle(`
        .bg-promotion,
        .bg-warning[role="alert"],
        .tip-box {
            display: none;
        }
    `);
    // language=CSS
    GM.addStyle(`
        .topnav-listItem:has([href*="/upgrade"]),
        .topnav-listItem:has([href*="/upgrade"]) + .topnav-vr,
        .topnav-listItem:has([data-testid^="cta"]),
        .dropdown-item:has([href="https://shop.proton.me"]),
        .navigation-item:has([href*="/upgrade"]),
        .modal-two:has([class*="offer"], [class*="deal"], [class*="free"]),
        .modal-two-backdrop:has(+ .modal-two [class*="offer"]),
        .modal-two-backdrop:has(+ .modal-two [class*="deal"]),
        .modal-two-backdrop:has(+ .modal-two [class*="free"]) {
            display: none;
        }
    `);
}
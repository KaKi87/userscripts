[← Homepage](../README.md)

# ProtonMail without annoyances

Remove annoying ProtonMail buttons & banners.

| [Install](../../../../raw/branch/master/protonmailWithoutAnnoyances/main.user.js) |
|-----------------------------------------------------------------------------------|

| Before                 | After                  |
|------------------------|------------------------|
| ![](./screenshot1.png) | ![](./screenshot2.png) |

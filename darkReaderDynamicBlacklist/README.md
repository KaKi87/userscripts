[← Homepage](../README.md)

# Dark Reader dynamic blacklist

Dynamically disable [Dark Reader](https://github.com/darkreader/darkreader) on already dark websites using the [`darkreader-lock` meta tag](https://github.com/darkreader/darkreader/pull/9181).

| [Install](../../../../raw/branch/master/darkReaderDynamicBlacklist/main.user.js) |
|----------------------------------------------------------------------------------|

## Use cases

### System-enabled dark theme

Websites featuring a dark theme but only enabled when [`prefers-color-scheme`](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme) is `dark` are forbidden from the global dark list as per the [contributing guidelines](https://github.com/darkreader/darkreader/blob/main/CONTRIBUTING.md#adding-a-website-that-is-already-dark).

### User-enabled dark theme

Websites featuring a dark theme but only enabled by user action.

In addition to disabling Dark Reader, the userscript enables the native dark theme.

### Multiple themes

Websites featuring multiple themes, including dark one.

In addition to disabling Dark Reader, the userscript provides an "Open theme switcher" option :

![](./screenshot1.png)

### Advanced situations

Websites featuring a dark theme, with exceptions (e.g. only available when logged in).

The userscript toggles Dark Reader and provides the "Open theme switcher" option whenever necessary.

## [Contributing guidelines](./CONTRIBUTING.md)

## Changelog

**Note :** for this specific project, the patch version number will only be incremented for site list updates, and considering how often those happen, those won't be documented.

* `0.1.0` (2023-01-30) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/30836d23933eef98c38728628994118a2d7f0d62/darkReaderDynamicBlacklist)
* `0.2.0` (2023-02-03) • [Fix Firefox support](https://git.kaki87.net/KaKi87/userscripts/src/commit/2c2833b9a3b2e21dbe6e992fcddffa2df4dc0592/darkReaderDynamicBlacklist)
* `0.3.0` (2023-02-05) • [Prevent observer start after domain-based action](https://git.kaki87.net/KaKi87/userscripts/src/commit/b707d9d8fcbcb9b2116dde23054f1129c41ad42c/darkReaderDynamicBlacklist)
* `0.4.0` (2023-07-16) • Fix light mode blinking before dark mode switching
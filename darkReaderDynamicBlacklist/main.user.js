// ==UserScript==
// @match       *://*/*
// @name        Dark Reader dynamic blacklist
// @description Dynamically disable Dark Reader on already dark websites
// @grant       GM.registerMenuCommand
// @grant       GM.addElement
// @version     0.4.10
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/darkReaderDynamicBlacklist
// @run-at      document-start
// @require     https://git.kaki87.net/KaKi87/userscripts/raw/commit/0da5a1c5d535fb40588d1c421170fe79790e7724/_lib/waitForSelector.js
// @require     https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js
// ==/UserScript==

let isStartObserver = true;

const
    /**
     * Prevent the observer from starting.
     */
    preventObserver = () => {
        isStartObserver = false;
        console.info({ isStartObserver });
    },
    /**
     * Wait for `DOMContentLoaded`.
     * @returns {Promise}
     */
    onDom = () => new Promise(resolve => document.addEventListener('DOMContentLoaded', resolve)),
    /**
     * Disable Dark Reader.
     */
    disableDarkReader = () => {
        (async () => {
            const darkReaderDisableElement = document.createElement('meta');
            darkReaderDisableElement.setAttribute('name', 'darkreader-lock');
            if(!document.head)
                await onDom();
            document.head.appendChild(darkReaderDisableElement);
            console.info({ darkReaderDisableElement });
        })();
    },
    /**
     * Implement the "Open theme switcher" feature.
     * @param {function} callback
     */
    setThemeSwitcherOpener = callback => GM.registerMenuCommand('Open theme switcher', callback),
    /**
     * Wait for the `document.readyState` = `complete` event.
     * @returns {Promise}
     */
    onComplete = () => new Promise(resolve => {
        if(document.readyState === 'complete')
            resolve();
        else
            document.addEventListener(
                'readystatechange',
                () => {
                    if(document.readyState === 'complete')
                        resolve();
                }
            );
    }),
    /**
     * Set a cookie & refresh page when necessary.
     * @param {string} key
     * @param {string} value
     */
    setCookie = (key, value) => {
        const isSet = Cookies.get(key) === value;
        if(!isSet){
            Cookies.set(key, value);
            window.stop();
            window.location.reload();
        }
        return isSet;
    },
    /**
     * Check whether an element has any of the mentioned classes.
     * @param {HTMLElement} element
     * @param {string[]} classes
     * @returns {boolean}
     */
    hasAnyClass = (element, classes) => classes.some(_ => element.classList.contains(_)),
    startsWithAny = (string, substrings) => substrings.some(substring => string.startsWith(substring));

switch(window.location.hostname){
    // Special
    case 'localhost': /* Development purposes */
    case 'documentserver.kdrive.infomaniak.com': /* Formatted text editor */
    case 'jsfiddle.net': /* Dark domain but light subdomains */
    case 'fiddle.jshell.net': /* Code snippet previewer */
    case 'www.siteone.io': /* Dark domain but system subdomains */
    case 'www.zebkit.org': /* Mandatory manual theme selection */
    // System
    case '512kb.club':
    case 'blog.androz2091.fr':
    case 'arrowverse.info':
    case 'oql.avris.it':
    case 'boltcss.com':
    case 'campground.bonfire.cafe':
    case 'borderleft.com':
    case 'code.golf':
    case 'hunt.deno.land':
    case 'dioxuslabs.com':
    case 'distrobox.it':
    case 'flathub.org':
    case 'flatpak.org':
    case 'docs.github.com':
    case 'gist.github.com':
    case 'docs.hoppscotch.io':
    case 'imgflip.com':
    case 'is-an.app':
    case 'jcs.org':
    case 'joinfirefish.org':
    case 'api.kde.org':
    case 'develop.kde.org':
    case 'korben.info':
    case 'www.lalutineduweb.fr':
    case 'leonardocolor.io':
    case 'meiert.com':
    case 'learn.microsoft.com':
    case 'interactive-examples.mdn.mozilla.net':
    case 'pavi2410.me':
    case 'pinafore.social':
    case 'prismlauncher.org':
    case 'status.projectsegfau.lt':
    case 'qwik.dev':
    case 'simplecss.org':
    case 'web.telegram.org':
    case 'twinery.org':
    case 'm.twitch.tv':
    case 'typeorm.io':
    case 'nitro.unjs.io':
    case 'vincelinise.com':
    case 'vivaldi.com':
    case 'help.vivaldi.com':
    case 'vivaldi.net':
    case 'themes.vivaldi.net':
    case 'webhook.site':
    case 'xeiaso.net':
    case 'yeezymafia.com':
    case 'zod.dev':
    // System or user (binary)
    case 'react-spectrum.adobe.com':
    case 'www.agnosticui.com':
    case 'aplcart.info':
    case 'arewewaylandyet.com':
    case 'v2.baklava.tech':
    case 'beautifier.io':
    case 'bedrock.dev':
    case 'wiki.bedrock.dev':
    case 'bestofjs.org':
    case 'blobs.gg':
    case 'boxicons.com':
    case 'search.brave.com':
    case 'capacities.io':
    case 'changewindows.org':
    case 'blog.codeberg.org':
    case 'forgejo.codeberg.page':
    case 'www.cosmicjs.com':
    case 'css.gg':
    case 'docs.cssninja.io':
    case 'daisyui.com':
    case 'docs.directus.io':
    case 'support.dnsimple.com':
    case 'docs.docker.com':
    case 'edit.photo':
    case 'www.electronjs.org':
    case 'element-plus.org':
    case 'eslint.org':
    case 'fakerjs.dev':
    case 'feathericons.com':
    case 'flameshot.org':
    case 'flowbite.com':
    case 'flume.dev':
    case 'forgejo.org':
    case 'www.frandroid.com':
    case 'gethomepage.dev':
    case 'd3ward.github.io':
    case 'gitmoji.dev':
    case 'goauthentik.io':
    case 'godbolt.org':
    case 'fonts.google.com':
    case 'harlemjs.com':
    case 'ui.adsabs.harvard.edu':
    case 'hexed.it':
    case 'hoppscotch.io':
    case 'vue-i18n.intlify.dev':
    case 'jamstack.org':
    case 'javascript.info':
    case 'ar.javascript.info':
    case 'es.javascript.info':
    case 'fa.javascript.info':
    case 'fr.javascript.info':
    case 'id.javascript.info':
    case 'it.javascript.info':
    case 'ja.javascript.info':
    case 'ko.javascript.info':
    case 'tr.javascript.info':
    case 'uk.javascript.info':
    case 'zh.javascript.info':
    case 'youtrack.jetbrains.com':
    case 'www.jsonvieweronline.com':
    case 'jvarchive.com':
    case 'discord.js.org':
    case 'keystonejs.com':
    case 'uso.kkx.one':
    case 'knexjs.org':
    case 'watercss.kognise.dev':
    case 'librewolf.net':
    case 'outlook.live.com':
    case 'louisderrac.com':
    case 'www.lttstore.com':
    case 'mailcow.email':
    case 'docs.mattermost.com':
    case 'modernorange.io':
    case 'modrinth.com':
    case 'developer.mozilla.org':
    case 'www.naiveui.com':
    case 'desktop-ui.netlify.app':
    case 'evite.netlify.app':
    case 'tailbliss.netlify.app':
    case 'horizon.netscout.com':
    case 'nextjs.org':
    case 'docs.nodegui.org':
    case 'vue.nodegui.org':
    case 'nodejs.org':
    case 'www.numerama.com':
    case 'nuxt.com':
    case 'ohdear.app':
    case 'www.op.gg':
    case 'overreacted.io':
    case 'parceljs.org':
    case 'phcode.dev':
    case 'picocss.com':
    case 'sharp.pixelplumbing.com':
    case 'pnpm.io':
    case 'docs.postalserver.io':
    case 'praz.dev':
    case 'discuss.privacyguides.net':
    case 'projectsegfau.lt':
    case 'pulsar-edit.dev':
    case 'toga.readthedocs.io':
    case 'regex101.com':
    case 'rentry.co':
    case 'blog.replit.com':
    case 'scribe.rip':
    case 'shoelace.style':
    case 'crawler.siteone.io':
    case 'sli.dev':
    case 'spring.io':
    case 'www.startpage.com':
    case 'support.stats.fm':
    case 'www.stedi.com':
    case 'textwizardai.streamlit.app':
    case 'svelte.dev':
    case 'kit.svelte.dev':
    case 'learn.svelte.dev':
    case 'symfony.com':
    case 'tableconvert.com':
    case 'tauri.app':
    case 'beta.tauri.app':
    case 'the-guild.dev':
    case 'developer.themoviedb.org':
    case 'www.twitch.tv':
    case 'ubuntuunity.org':
    case 'ultramarine-linux.org':
    case 'unityd.org':
    case 'vercel.com':
    case 'fastify-nuxt.vercel.app':
    case 'fluent-svelte.vercel.app':
    case 'radash-docs.vercel.app':
    case 'vitejs.dev':
    case 'v3.vitejs.dev':
    case 'docs.vocata.one':
    case 'vuejs.org':
    case 'play.vuejs.org':
    case 'vueuse.org':
    case 'docs.weblate.org':
    case 'yann.pt':
    case 'www.youtube.com':
    case 'zitadel.com':
    // System or user (non-binary)
    case 'easypodcasts.live':
    case 'giscus.app':
    case 'github.dev':
    case 'lidraughts.org': {
        preventObserver();
        disableDarkReader();
        break;
    }
    case 'github.com': {
        preventObserver();
        if(!window.location.pathname.startsWith('/about')){
            disableDarkReader();
            setThemeSwitcherOpener(() => window.location.assign('/settings/appearance'));
        }
        break;
    }
    // User (binary)
    case 'alternativeto.net':
    case 'create-react-app.dev':
    case 'orm.drizzle.team':
    case 'getbootstrap.com':
    case 'fastify.dev':
    case 'stable.getautoclicker.com':
    case 'invidious.io':
    case 'leo.fm':
    case 'lowendtalk.com':
    case 'discordeno.mod.land':
    case 'nyaa.si':
    case 'www.online-cpp.com':
    case 'pptr.dev':
    case 'jestjs.io':
    case 'eta.js.org':
    case 'redux.js.org':
    case 'upptime.js.org':
    case 'shields.io':
    case 'docs.strapi.io':
    case 'auth.tedomum.net': {
        preventObserver();
        localStorage.setItem('theme', 'dark');
        disableDarkReader();
        break;
    }
    case 'alldebrid.com':
    case 'www.mycompiler.io':
    case 'yggland.fr': {
        preventObserver();
        if(setCookie('theme', 'dark'))
            disableDarkReader();
        break;
    }
    case 'henry.codes':
    case 'siastats.info': {
        preventObserver();
        localStorage.setItem('darkMode', 'true');
        disableDarkReader();
        break;
    }
    case 'catbox.moe':
    case 'litterbox.catbox.moe': {
        preventObserver();
        if(setCookie('darktheme', '1'))
            disableDarkReader();
        break;
    }
    case 'www.beercss.com': {
        preventObserver();
        disableDarkReader();
        waitForSelector('#top-app-bar1 button:nth-child(2)').then(_ => _.click());
        break;
    }
    case 'chakra-ui.com': {
        preventObserver();
        localStorage.setItem('chakra-ui-color-mode', 'dark');
        disableDarkReader();
        break;
    }
    case 'v0.chakra-ui.com': {
        preventObserver();
        disableDarkReader();
        onComplete().then(() => document.querySelector('.css-1rgn53n').click());
        break;
    }
    case 'vue.chakra-ui.com': {
        preventObserver();
        localStorage.setItem('chakra_ui_docs_color_mode', 'dark');
        disableDarkReader();
        break;
    }
    case 'www.cinemablind.com': {
        preventObserver();
        localStorage.setItem('g1_skin', 'dark');
        disableDarkReader();
        break;
    }
    case 'classless.de': {
        preventObserver();
        disableDarkReader();
        onDom().then(() => unsafeWindow['toggleDarkMode'](document.querySelector('[onclick="toggleDarkMode(this)"]')));
        break;
    }
    case 'crdroid.net': {
        preventObserver();
        disableDarkReader();
        localStorage.setItem('toggle-bootstrap-theme', '{"isDark":true}');
        break;
    }
    case 'www.devtoolsdaily.com':
    case 'beta.devtoolsdaily.com': {
        preventObserver();
        localStorage.setItem('mantine-color-scheme-value', 'dark');
        disableDarkReader();
        break;
    }
    case 'docs.rs': {
        preventObserver();
        localStorage.setItem('rustdoc-theme', 'dark');
        disableDarkReader();
        break;
    }
    case 'www.fosslinux.com': {
        preventObserver();
        if(setCookie('penci_mode', 'dark'));
            disableDarkReader();
        break;
    }
    case 'gregfresnel.free.fr': {
        preventObserver();
        localStorage.setItem('darkmode', 'dark');
        disableDarkReader();
    }
    case 'gitexplorer.com': {
        preventObserver();
        localStorage.setItem('dark', 'true');
        disableDarkReader();
        break;
    }
    case 'issuetracker.google.com': {
        preventObserver();
        localStorage.setItem('onedev-darkmode', 'true');
        disableDarkReader();
        break;
    }
    case 'keep.google.com': {
        preventObserver();
        disableDarkReader();
        break;
    }
    case 'onecompiler.com': {
        preventObserver();
        localStorage.setItem('reduxState', '{"user":{"version":"v4","theme":"dark"}}');
        disableDarkReader();
        break;
    }
    case 'code.onedev.io': {
        preventObserver();
        if(setCookie('darkMode', 'yes'))
            disableDarkReader();
        break;
    }
    case 'www.paste.photos': {
        preventObserver();
        localStorage.setItem('vueuse-color-scheme', 'dark');
        disableDarkReader();
        break;
    }
    case 'get.pixelexperience.org': {
        preventObserver();
        if(setCookie('pixel-experience-theme', 'dark'))
            disableDarkReader();
        break;
    }
    case 'www.pornpics.com': {
        preventObserver();
        if(setCookie('PP_THEME', '1'))
            disableDarkReader();
        break;
    }
    case 'predb.me': {
        preventObserver();
        if(setCookie('scheme', '1'))
            disableDarkReader();
        break;
    }
    case 'www.rockmods.net': {
        preventObserver();
        localStorage.setItem('mode', 'darkmode');
        disableDarkReader();
        break;
    }
    case 'app.slack.com': {
        preventObserver();
        disableDarkReader();
        setThemeSwitcherOpener(async () => {
            document.querySelector('[data-qa="user-button"]').click();
            (await waitForSelector('[id^="menu-"][id$="-6"]')).click();
            (await waitForSelector('#themes')).click();
        });
        break;
    }
    case 'docs.theme-park.dev': {
        preventObserver();
        localStorage.setItem('/.__palette', '{"index":1,"color":{"scheme":"slate","primary":"red","accent":"light-green"}}');
        disableDarkReader();
        break;
    }
    case 'transform.tools': {
        preventObserver();
        localStorage.setItem('__transform_tools_isDarkMode', 'true');
        break;
    }
    case 'docs.vrchat.com': {
        preventObserver();
        localStorage.setItem('color-scheme', 'dark');
        disableDarkReader();
        break;
    }
    case 'www.xvideos.com': {
        preventObserver();
        onComplete().then(async () => {
            const themeSwitcherButton = await waitForSelector('#site-theme-switch');
            themeSwitcherButton.click();
            const darkButton = await waitForSelector('[data-mode="black"]');
            themeSwitcherButton.click();
            if(!darkButton.parentElement.classList.contains('current'))
                darkButton.click();
        });
        break;
    }
    // User (non-binary)
    case 'anilist.co': {
        preventObserver();
        localStorage.setItem('site-theme', 'dark');
        disableDarkReader();
        break;
    }
    case 'mail.infomaniak.com': {
        preventObserver();
        disableDarkReader();
        setThemeSwitcherOpener(() => document.querySelector('[aria-describedby="cdk-describedby-message-3"]').click());
        break;
    }
    case 'mail.proton.me': {
        preventObserver();
        disableDarkReader();
        setThemeSwitcherOpener(async () => {
            document.querySelector('[data-test-id="view:general-settings"]').click();
            (await waitForSelector('#dropdown-17 .dropdown-item:nth-child(4) button')).click();
        });
        break;
    }
    case 'mail.tutanota.com': {
        preventObserver();
        localStorage.setItem(
            'tutanotaConfig',
            JSON.stringify({
                ...JSON.parse(localStorage.getItem('tutanotaConfig')),
                '_themeId': 'dark'
            })
        );
        disableDarkReader();
        break;
    }
    case 'vsthemes.org': {
        preventObserver();
        if(setCookie('theme__night', '1'))
            disableDarkReader();
        break;
    }
    // Advanced
    case 'www.betaseries.com': {
        preventObserver();
        if(Cookies.get('login')){
            disableDarkReader();
            setThemeSwitcherOpener(() => window.location.assign('/compte/theme'));
        }
        break;
    }
    case 'bun.sh': {
        preventObserver();
        if(window.location.pathname === '/' || window.location.pathname.startsWith('/docs'))
            disableDarkReader();
        break;
    }
    case 'caddyserver.com': {
        preventObserver();
        if(!['/', '/download'].includes(window.location.pathname))
            disableDarkReader();
    }
    case 'www.codingame.com': {
        preventObserver();
        if(window.location.pathname.startsWith('/ide'))
            disableDarkReader();
        break;
    }
    case 'deno.com': {
        preventObserver();
        if(!window.location.pathname.startsWith('/blog'))
            disableDarkReader();
        break;
    }
    case 'dietpi.com': {
        preventObserver();
        if(!startsWithAny(window.location.pathname, ['/blog', '/docs']))
            disableDarkReader();
        break;
    }
    case 'www.figma.com': {
        preventObserver();
        if(startsWithAny(window.location.pathname, ['/files', '/design']))
            disableDarkReader();
        break;
    }
    case 'ndragun92.github.io': {
        preventObserver();
        if(window.location.pathname.startsWith('/portfolio') && setCookie('theme', 'dark'))
            disableDarkReader();
        break;
    }
    case 'prazdevs.github.io': {
        if(window.location.pathname.startsWith('/pinia-plugin-persistedstate'))
            disableDarkReader();
        break;
    }
    case 'messages.google.com': {
        preventObserver();
        if(window.location.pathname.startsWith('/web'))
            disableDarkReader();
        if(window.location.pathname.startsWith('/web/conversations')){
            setThemeSwitcherOpener(async () => {
                document.querySelector('[data-e2e-main-nav-menu]').click();
                (await waitForSelector('[data-e2e-main-nav-menu="SETTINGS"]')).click();
            });
        }
        break;
    }
    case 'www.resultats-elections.interieur.gouv.fr': {
        preventObserver();
        localStorage.setItem('scheme', 'dark');
        disableDarkReader();
        break;
    }
    case 'kdrive.infomaniak.com': {
        preventObserver();
        if(!window.location.pathname.startsWith('/app/share')){
            disableDarkReader();
            setThemeSwitcherOpener(() => document.querySelector('.basic-list__item--settings button').click());
        }
        break;
    }
    case 'www.jetbrains.com': {
        preventObserver();
        if(window.location.pathname.startsWith('/help'))
            disableDarkReader();
        break;
    }
    case 'www.linkedin.com': {
        preventObserver();
        if(setCookie('li_theme', 'dark')){
            onComplete().then(() => {
                if(hasAnyClass(
                    document.documentElement,
                    [
                        'theme--dark',
                        'theme--dark-lix'
                    ]
                )){
                    disableDarkReader();
                }
                if(localStorage.getItem('C_C_M'))
                    setThemeSwitcherOpener(() => window.location.assign('/mypreferences/m/dark-mode'));
            });
        }
        break;
    }
    case 'mailfence.com': {
        preventObserver();
        if(window.location.pathname.startsWith('/flatx'))
            disableDarkReader();
        break;
    }
    case 'next.ink': {
        preventObserver();
        onDom().then(() => {
            if(document.querySelector('.profil_contenaire')){
                disableDarkReader();
                setThemeSwitcherOpener(async () => window.location.assign('https://next.ink/profile/'));
            }
        });
        break;
    }
    case 'www.notion.so': {
        preventObserver();
        onDom().then(() => {
            if(document.body.classList.contains('notion-body'))
                disableDarkReader();
        });
        break;
    }
    case 'www.programiz.com': {
        preventObserver();
        if(window.location.pathname.endsWith('/online-compiler/'))
            disableDarkReader();
        break;
    }
    case 'account.proton.me': {
        preventObserver();
        if(window.location.pathname.startsWith('/u')){
            disableDarkReader();
            setThemeSwitcherOpener(() => document.querySelector('[href$="/mail/appearance"]').click());
        }
        break;
    }
    case 'lite.qwant.com': {
        preventObserver();
        if(new URL(window.location).searchParams.get('theme') === '1')
            disableDarkReader();
        break;
    }
    case 'www.qwant.com': {
        preventObserver();
        if(
            window.location.pathname === '/'
            &&
            (
                Cookies.get('home')
                ||
                new URL(window.location).searchParams.get('q')
            )
        ){
            disableDarkReader();
        }
        break;
    }
    case 'old.reddit.com': {
        preventObserver();
        if(window.location.pathname.startsWith('/chat'))
            disableDarkReader();
        break;
    }
    case 'securitytrails.com': {
        preventObserver();
        if(window.location.pathname.startsWith('/app'))
            disableDarkReader();
        break;
    }
    case 'sequelize.org': {
        preventObserver();
        localStorage.setItem('theme', 'dark');
        if(!startsWithAny(
            window.location.pathname,
            [
                '/api/v6',
                '/v5',
                '/v4',
                '/v3',
                '/v2',
                '/v1'
            ]
        )){
            disableDarkReader();
        }
        break;
    }
    case 'www.skypack.dev': {
        preventObserver();
        localStorage.setItem('darktheme', 'dark');
        if(!window.location.pathname.startsWith('/blog'))
            disableDarkReader();
        break;
    }
    case 'www.spotify.com': {
        preventObserver();
        if(!window.location.pathname.slice(3).startsWith('/download'))
            disableDarkReader();
        break;
    }
    case 'stackblitz.com': {
        preventObserver();
        if(startsWithAny(window.location.pathname, ['/register', '/sign_in', '/edit']))
            disableDarkReader();
        break;
    }
    case 'stackedit.io': {
        preventObserver();
        if(!localStorage.getItem('data/settings'))
            localStorage.setItem('data/settings', '{"id":"settings","type":"data","data":"colorTheme: dark\\n","hash":1189039882}');
        if(window.location.pathname === '/app')
            disableDarkReader();
        break;
    }
    case 'survey.stackoverflow.co': {
        preventObserver();
        if(!['/', '/2015', '/2016', '/2017', '/2018', '/2019'].includes(window.location.pathname))
            disableDarkReader();
        break;
    }
    case 'tinder.com': {
        preventObserver();
        if(window.location.pathname.startsWith('/app')){
            disableDarkReader();
            setThemeSwitcherOpener(() => window.location.assign('/app/settings'));
        }
        break;
    }
    case 'www.wordreference.com': {
        preventObserver();
        localStorage.setItem('sWRSettings', JSON.stringify({
            ...JSON.parse(localStorage.getItem('sWRSettings')),
            'theme': 'dark'
        }));
        if(window.location.pathname !== '/')
            disableDarkReader();
        break;
    }
    case 'xel-toolkit.org': {
        preventObserver();
        const value = localStorage.getItem('theme');
        if(!value?.endsWith('-dark'))
            localStorage.setItem('theme', `${value || 'adwaita'}-dark`);
        disableDarkReader();
        break;
    }
}

let
    observer,
    isObserverStopped,
    og,
    noscript,
    manifest,
    generator,
    iosTitle;

const
    stopObserver = () => {
        observer.disconnect();
        isObserverStopped = true;
        console.info({ isObserverStopped });
    },
    observerCallback = async () => {
        // Self-hosted
        if(!og){
            og = document.querySelector('[property^="og:"]')
                ? [...document.querySelectorAll('[property^="og:"]')].reduce((og, element) => ({
                    ...og,
                    [element.getAttribute('property').slice(3)]:
                        element.getAttribute('content')
                }), {})
                : undefined;
            if(og){
                switch(og['title']){
                    // [Cinny](https://github.com/cinnyapp/cinny)
                    case 'Cinny': {
                        stopObserver();
                        if(window.location.origin !== og['url']){
                            disableDarkReader();
                            setThemeSwitcherOpener(() => document.querySelector('.sticky-container .sidebar-avatar:last-child').click());
                        }
                        break;
                    }
                    // [Discord](https://discord.com)
                    case 'Discord - Group Chat That’s All Fun & Games':
                    // [Pony House](https://github.com/Pony-House/Client)
                    case 'Pony House':
                    // [Variance](https://github.com/mat-1/variance)
                    case 'Variance': {
                        stopObserver();
                        disableDarkReader();
                        break;
                    }
                }
                switch(og['site_name']){
                    // [Elk](https://github.com/elk-zone/elk)
                    case 'Elk': {
                        stopObserver();
                        disableDarkReader();
                        setThemeSwitcherOpener(() => window.location.assign('/settings/interface'));
                        break;
                    }
                    // [GitLab](https://gitlab.com/gitlab-org/gitlab)
                    case 'GitLab': {
                        stopObserver();
                        await onDom();
                        if(document.querySelector('.header-user-dropdown-toggle, [data-testid="user-dropdown"]')){
                            disableDarkReader();
                            setThemeSwitcherOpener(() => window.location.assign('/-/profile/preferences'));
                        }
                        break;
                    }
                    // [Netdata](https://github.com/netdata/netdata)
                    case 'netdata': {
                        stopObserver();
                        disableDarkReader();
                        break;
                    }
                    // [Nitter](https://github.com/zedeus/nitter)
                    case 'Nitter': {
                        stopObserver();
                        if(setCookie('theme', 'Nitter'))
                            disableDarkReader();
                        break;
                    }
                }
                console.info({ og });
            }
        }
        if(!noscript){
            noscript = document.querySelector('noscript')?.textContent;
            if(noscript){
                switch(noscript){
                    // [GL.iNet Admin Panel](https://www.gl-inet.com/)
                    case '<strong>We\'re sorry but gl-ui doesn\'t work properly without JavaScript enabled. Please enable it to continue.</strong>': {
                        stopObserver();
                        disableDarkReader();
                        break;
                    }
                    // [Pleroma](https://git.pleroma.social/pleroma/pleroma)
                    case 'To use Pleroma, please enable JavaScript.': {
                        stopObserver();
                        disableDarkReader();
                        setThemeSwitcherOpener(async () => {
                            document.querySelector('.inner-nav .actions > button').click();
                            (await waitForSelector('.settings_tab-switcher > .tabs .tab-wrapper:nth-child(3) button')).click();
                        });
                        break;
                    }
                }
                console.info({ noscript });
            }
        }
        {
            const manifestUrl = document.querySelector('[rel="manifest"]')?.href;
            if(manifestUrl){
                if(!manifest){
                    manifest = (async () => await (await fetch(manifestUrl)).json())();
                    manifest = await manifest;
                    console.log({ manifest });
                }
                switch(manifest.name){
                    // [code-server](https://github.com/coder/code-server)
                    case 'code-server': {
                        stopObserver();
                        if(window.location.pathname !== '/login')
                            disableDarkReader();
                        break;
                    }
                    // [Flood](https://github.com/jesec/flood)
                    case 'Flood':
                    // [HedgeDoc](https://github.com/hedgedoc/hedgedoc)
                    case 'HedgeDoc':
                    // [Invidious](https://github.com/iv-org/invidious)
                    case 'Invidious': {
                        stopObserver();
                        disableDarkReader();
                        break;
                    }
                    // [Kbin](https://github.com/ernestwisniewski/kbin)
                    case '/kbin': {
                        stopObserver();
                        if(setCookie('kbin_theme', 'dark'))
                            disableDarkReader();
                        break;
                    }
                    // [KeeWeb](https://github.com/keeweb/keeweb)
                    case 'KeeWeb': {
                        stopObserver();
                        await onComplete();
                        await new Promise(resolve => setTimeout(resolve, 0));
                        if(hasAnyClass(
                            document.body,
                            [
                                'th-dark',
                                'th-sd',
                                'th-fb',
                                'th-db',
                                'th-te',
                                'th-dc'
                            ]
                        )){
                            disableDarkReader();
                        }
                        break;
                    }
                    // [Libreddit](https://github.com/spikecodes/libreddit)
                    case 'Libreddit': {
                        stopObserver();
                        if(setCookie('theme', 'dark'))
                            disableDarkReader();
                        break;
                    }
                    // [Mattermost](https://github.com/mattermost/mattermost)
                    case 'Mattermost': {
                        if(document.body.classList.contains('app__body')){
                            stopObserver();
                            disableDarkReader();
                        }
                        break;
                    }
                    // [Nextcloud](https://github.com/nextcloud/server)
                    case 'Nextcloud':
                    // [Photon](https://github.com/Xyphyn/photon)
                    case 'Photon for Lemmy': {
                        stopObserver();
                        disableDarkReader();
                        break;
                    }
                    // [Pixelfed](https://github.com/pixelfed/pixelfed)
                    case 'Pixelfed': {
                        stopObserver();
                        if(window.location.pathname.startsWith('/i')){
                            disableDarkReader();
                            setThemeSwitcherOpener(() => document.querySelector('[aria-labelledby="navbarDropdown"] .nav-item:nth-child(5) a').click());
                        }
                        break;
                    }
                    // [Quetre](https://github.com/zyachel/quetre)
                    case 'Quetre': {
                        stopObserver();
                        localStorage.setItem('theme', 'dark');
                        disableDarkReader();
                        break;
                    }
                    // [Upptime](https://github.com/upptime/upptime)
                    case 'Upptime': {
                        stopObserver();
                        if(document.querySelector(['dark', 'night'].map(_ => `[href$="/themes/${_}.css"]`).join(',')))
                            disableDarkReader();
                        break;
                    }
                    // [Weblate](https://github.com/WeblateOrg/weblate)
                    case 'Weblate': {
                        stopObserver();
                        disableDarkReader();
                        break;
                    }
                    // [Wiki.js](https://github.com/requarks/wiki)
                    case 'Wiki.js': {
                        stopObserver();
                        if(window.location.pathname !== '/login'){
                            if(unsafeWindow['siteConfig']['darkMode'])
                                disableDarkReader();
                            setThemeSwitcherOpener(() => window.location.assign('/a/theme'));
                        }
                        break;
                    }
                }
            }
        }
        if(!generator){
            generator = document.querySelector('[name="generator"]')?.getAttribute('content');
            if(generator){
                // [cState](https://github.com/cstate/cstate)
                if(generator.startsWith('cState')){
                    stopObserver();
                    await onComplete();
                    if(window.getComputedStyle(document.body).getPropertyValue('background-color') !== 'rgb(24, 26, 27)')
                        disableDarkReader();
                }
                // [Discourse](https://github.com/discourse/discourse)
                if(generator.startsWith('Discourse')){
                    stopObserver();
                    if(document.querySelector('#data-discourse-setup[data-color-scheme-is-dark="true"]'))
                        disableDarkReader();
                }
                // [Docusaurus](https://github.com/facebook/docusaurus)
                if(generator.startsWith('Docusaurus')){
                    stopObserver();
                    if(![
                        'yarnpkg.com',
                        'superset.apache.org'
                    ].includes(window.location.hostname)){
                        localStorage.setItem('theme', 'dark');
                        disableDarkReader();
                    }
                }
                // [Matomo](https://github.com/matomo-org/matomo)
                if(generator === 'Matomo - free/libre analytics platform'){
                    stopObserver();
                    disableDarkReader();
                }
                // [SearXNG](https://github.com/searxng/searxng)
                if(generator.startsWith('searxng')){
                    stopObserver();
                    disableDarkReader();
                }
                // [VitePress](https://github.com/vuejs/vitepress)
                if(generator.startsWith('VitePress')){
                    stopObserver();
                    disableDarkReader();
                }
                console.info({ generator });
            }
        }
        if(!iosTitle){
            iosTitle = document.querySelector('[name="apple-mobile-web-app-title"]')?.getAttribute('content');
            if(iosTitle){
                switch(iosTitle){
                    // [Nextcloud](https://github.com/nextcloud/server)
                    case 'Nextcloud': {
                        stopObserver();
                        disableDarkReader();
                        break;
                    }
                }
                console.info({ iosTitle });
            }
        }
        {
            // [AdGuard Home](https://github.com/AdguardTeam/AdGuardHome)
            if(document.querySelector('[href="https://link.adtidy.org/forward.html?action=home&from=ui&app=home"]')){
                stopObserver();
                disableDarkReader();
            }
            // [AnonymousOverflow](https://github.com/httpjamesm/AnonymousOverflow)
            if(document.querySelector('img.logo[src="/static/codecircles.webp"]')){
                stopObserver();
                disableDarkReader();
            }
            // [AriaNg](https://github.com/mayswind/AriaNg)
            if(document.documentElement.getAttribute('ng-app') === 'ariaNg'){
                stopObserver();
                disableDarkReader();
                setThemeSwitcherOpener(() => document.querySelector('[href="#!/settings/ariang"]').click());
            }
            // [BreezeWiki](https://gitdab.com/cadence/breezewiki)
            if(document.querySelector('.fandom-community-header__background')){
                stopObserver();
                if(window.location.pathname !== '/' && setCookie('theme', 'dark'))
                    disableDarkReader();
            }
            // [Caddy](https://caddyserver.com)
            if(document.querySelector('footer a[rel="noopener noreferrer"][href="https://caddyserver.com"]')){
                stopObserver();
                disableDarkReader();
            }
            // [changedetection.io](https://github.com/dgtlmoon/changedetection.io)
            if(document.querySelector('[title="Changedetection.io » Feed"]')){
                stopObserver();
                if(setCookie('css_dark_mode', 'true'))
                    disableDarkReader();
            }
            // [Element](https://github.com/vector-im/element-web)
            if(document.querySelector('#matrixchat')){
                stopObserver();
                disableDarkReader();
            }
            // [Forgejo](https://codeberg.org/forgejo/forgejo)
            if(document.querySelector('[href$="/explore/repos"]')){
                const themes = [
                    'auto',
                    'arc-green',
                    'forgejo-auto',
                    'forgejo-dark',
                    'codeberg-auto',
                    'dark-arc',
                    'gitdotgay'
                ];
                if(
                    hasAnyClass(
                        document.documentElement,
                        [
                            'theme-',
                            ...themes.map(theme => `theme-${theme}`)
                        ]
                    )
                    ||
                    themes.includes(document.documentElement.getAttribute('data-theme'))
                ){
                    stopObserver();
                    disableDarkReader();
                    setThemeSwitcherOpener(() => window.location.assign('/user/settings/appearance'));
                }
            }
            // [Google Search](https://www.google.com)
            if(document.querySelector('[href^="https://support.google.com/websearch/?p=ws_results_help"], #slim_appbar #result-stats, g-snackbar')){
                stopObserver();
                disableDarkReader();
            }
            // [JSON Viewer](https://github.com/pd4d10/json-viewer)
            if(document.querySelector('[href="https://github.com/pd4d10/json-viewer"]')){
                stopObserver();
                disableDarkReader();
            }
            // [Mastodon](https://github.com/mastodon/mastodon)
            if(
                document.querySelector('.app-holder#mastodon')
                ||
                (document.querySelector('body.no-reduce-motion, body.reduce-motion') && document.querySelector('[href="/auth/sign_in"], [href="/auth/sign_out"]'))
            ){
                stopObserver();
                const isLight = localStorage.getItem('darkReaderDynamicBlacklist:isLight');
                if(!isLight || isLight === 'false')
                    disableDarkReader();
                if(!isLight){
                    await Promise.all([
                        new Promise(resolve => GM
                            .addElement(
                                'script',
                                {
                                    'src': 'https://cdn.jsdelivr.net/gh/marcelodolza/iziToast@v1.4.0/dist/js/iziToast.min.js'
                                }
                            )
                            .addEventListener('load', resolve)
                        ),
                        new Promise(resolve => GM
                            .addElement(
                                'link',
                                {
                                    'rel': 'stylesheet',
                                    'href': 'https://cdn.jsdelivr.net/gh/marcelodolza/iziToast@v1.4.0/dist/css/iziToast.min.css'
                                }
                            )
                            .addEventListener('load', resolve)
                        )
                    ]);
                    unsafeWindow.iziToast.show({
                        theme: 'dark',
                        title: 'Is it light ?',
                        message: 'There is currently no automated way to know whether a Mastodon instance is light or dark.',
                        position: 'bottomRight',
                        buttons: [[
                            '<button>Yes</button>',
                            (instance, toast) => instance.hide(undefined, toast, 'button:yes')
                        ]],
                        onClosing: (_, __, reason) => {
                            const isLight = reason === 'button:yes';
                            localStorage.setItem(
                                'darkReaderDynamicBlacklist:isLight',
                                isLight.toString()
                            );
                            if(isLight)
                                window.location.reload();
                        }
                    });
                }
                setThemeSwitcherOpener(() => window.location.assign('/settings/preferences/appearance'));
            }
            // [NextCloud](https://github.com/nextcloud/server)
            if(unsafeWindow['_theme']?.['productName'] === 'Nextcloud'){
                stopObserver();
                disableDarkReader();
            }
            // [rimgo](https://codeberg.org/video-prize-ranch/rimgo)
            if(document.querySelector('[href="https://codeberg.org/video-prize-ranch/rimgo"], [href="https://codeberg.org/rimgo/rimgo"]')){
                stopObserver();
                disableDarkReader();
            }
            // [Roundcube Webmail](https://github.com/roundcube/roundcubemail)
            if(document.querySelector('[id^="rcmbtn"]')){
                stopObserver();
                if(document.documentElement.classList.contains('dark-mode'))
                    disableDarkReader();
            }
            // [Sentry](https://github.com/getsentry/self-hosted)
            if(window['__onSentryInit'] || document.querySelector('[href="https://sentry.io/welcome/"]')){
                if(startsWithAny(
                    window.location.pathname,
                    [
                        '/organizations',
                        '/settings',
                        '/manage'
                    ]
                )){
                    stopObserver();
                    disableDarkReader();
                    setThemeSwitcherOpener(() => window.location.assign('/settings/account/details/'));
                }
            }
            // [Speedtest Tracker](https://github.com/alexjustesen/speedtest-tracker)
            if(document.title.endsWith(' - Speedtest Tracker')){
                stopObserver();
                localStorage.setItem('theme', 'dark');
                disableDarkReader();
            }
            // [StackExchange](https://stackexchange.com/about)
            if(document.querySelector('[href="https://stackoverflow.com/help/licensing"]')){
                stopObserver();
                if(document.body.classList.contains('theme-dark'))
                    disableDarkReader();
                if(window.location.hostname === 'stackoverflow.com')
                    setThemeSwitcherOpener(() => window.location.assign('/users/preferences'));
            }
            // [Strapi](https://github.com/strapi/strapi)
            if(unsafeWindow['strapi']){
                stopObserver();
                disableDarkReader();
            }
            // [Lemmy](https://github.com/LemmyNet/lemmy)
            if(unsafeWindow['lemmyConfig'] || document.querySelector('.lemmy-site')){
                stopObserver();
                disableDarkReader();
            }
            // [Woodpecker](https://github.com/woodpecker-ci/woodpecker)
            if(unsafeWindow['WOODPECKER_VERSION']){
                stopObserver();
                disableDarkReader();
            }
        }
    };

const startObserver = () => {
    observer = new MutationObserver(observerCallback);
    observer.observe(
        document.head,
        {
            childList: true
        }
    );
    observerCallback().catch(console.error);
};

if(isStartObserver){
    if(document.head){
        startObserver();
        onDom().then(() => {
            if(!isObserverStopped)
                observerCallback().catch(console.error);
        });
    }
    else
        onDom().then(startObserver);
}

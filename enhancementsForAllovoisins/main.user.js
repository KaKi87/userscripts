// ==UserScript==
// @match         https://www.allovoisins.com/*
// @name          Enhancements for AlloVoisins
// @description   Enhancements for AlloVoisins
// @version       0.1.3
// @author        KaKi87
// @license       MIT
// @namespace     https://git.kaki87.net/KaKi87/userscripts/src/branch/master/enhancementsForAllovoisins
// @run-at        document-start
// @require       https://git.kaki87.net/KaKi87/userscripts/raw/commit/81b622e55a0b0f9599ac55e572d9ea9a71a6f038/_lib/createXhrInterceptor.js
// @require       https://cdn.jsdelivr.net/npm/emel
// ==/UserScript==

const redirectParam = new URL(window.location).searchParams.get('redirect_url_after_login');
if(redirectParam){
    const redirectUrl = new URL(redirectParam);
    redirectUrl.searchParams.set('at', JSON.parse(window.localStorage.getItem('vuex'))['user']['auth_token']);
    window.location.replace(redirectUrl);
}

const
    APP_ID = 'enhancementsForAllovoisins',
    xhrOpen = unsafeWindow.XMLHttpRequest.prototype.open,
    sentReviews = [],
    createSimpleElement = selector => window.emel(selector, { returnSingleChild: true }),
    profileElements = [];

let
    sentReviewsTabElement,
    profileUrl;

unsafeWindow.XMLHttpRequest.prototype.open = createXhrInterceptor({
    xhrOpen,
    onResponse: ({
        responseURL,
        responseText
    }) => {
        responseURL = new URL(responseURL);
        if(responseURL.href.startsWith('https://www.allovoisins.com/api/user')){
            let data;
            try { data = JSON.parse(responseText); } catch {}
            if(!data) return;
            if(responseURL.pathname.endsWith('/profile')){
                document.querySelector('.title-profile-seo h1').textContent = data['data']['profile']['full_name'];
            }
            if(responseURL.pathname.endsWith('/activities')){
                const profileId = responseURL.pathname.split('/').slice(-2)[0];
                sentReviews.push(...data['data'].filter(item => item['activity_type'] === 'review_sent'));
                if(sentReviews.length && !sentReviewsTabElement){
                    const tabsContainerElement = document.querySelector('.tabs-content');
                    sentReviewsTabElement = tabsContainerElement.lastChild.cloneNode(true);
                    sentReviewsTabElement.childNodes[0].textContent = 'Avis laissés';
                    sentReviewsTabElement.addEventListener('click', () => {
                        let reviewIndex = 0;
                        const
                            modalElement = createSimpleElement(`.__${APP_ID}__modal`),
                            modalIframeElement = createSimpleElement(`iframe.__${APP_ID}__modal__iframe`),
                            modalNavElement = createSimpleElement(`nav.__${APP_ID}__modal__nav`),
                            navStatusElement = createSimpleElement(`span.__${APP_ID}__modal__nav__status`),
                            updateModal = () => {
                                navStatusElement.textContent = `${reviewIndex + 1}/${sentReviews.length}`;
                                const reviewUrl = new URL(sentReviews[reviewIndex]['ref_url']);
                                reviewUrl.searchParams.set(`__${APP_ID}__reviewerProfileId`, profileId);
                                modalIframeElement.setAttribute('src', reviewUrl);
                            },
                            navPrevElement = createSimpleElement(`button.__${APP_ID}__modal__nav__button{Précédent}`),
                            navNextElement = createSimpleElement(`button.__${APP_ID}__modal__nav__button{Suivant}`),
                            navOpenElement = createSimpleElement(`button.__${APP_ID}__modal__nav__button{Ouvrir}`),
                            navCloseElement = createSimpleElement(`button.__${APP_ID}__modal__nav__button{Fermer}`);
                        updateModal();
                        modalElement.appendChild(modalIframeElement);
                        modalNavElement.appendChild(navStatusElement);
                        navPrevElement.addEventListener('click', () => {
                            if(reviewIndex === 0) return;
                            reviewIndex--;
                            updateModal();
                        });
                        modalNavElement.appendChild(navPrevElement);
                        navNextElement.addEventListener('click', () => {
                            if(reviewIndex === sentReviews.length - 1) return;
                            reviewIndex++;
                            updateModal();
                        });
                        modalNavElement.appendChild(navNextElement);
                        navOpenElement.addEventListener('click', () => window.open(sentReviews[reviewIndex]['ref_url'], '_blank'));
                        modalNavElement.appendChild(navOpenElement);
                        navCloseElement.addEventListener('click', () => modalElement.remove());
                        modalNavElement.appendChild(navCloseElement);
                        modalElement.appendChild(modalNavElement);
                        document.body.appendChild(modalElement);
                    });
                    tabsContainerElement.appendChild(sentReviewsTabElement);
                }
            }
            if(responseURL.pathname.endsWith('/reviews')){
                const reviewerProfileId = parseInt(new URL(window.location.href).searchParams.get(`__${APP_ID}__reviewerProfileId`));
                if(reviewerProfileId){
                    const
                        reviewsStyles = new CSSStyleSheet(),
                        reviewSelector = `:has(> #evaluation-${data['data'].find(item => item['reviewer']['id'] === reviewerProfileId)['id']})`;
                    // language=CSS
                    reviewsStyles.replaceSync(`
                        :has(> .block-reviews) {
                            display: flex;
                            flex-direction: column;
                        }
                        ${reviewSelector} {
                            order: -1;
                            outline: 2px solid red;
                        }
                        @media (hover: none) {
                            ${reviewSelector} {
                                outline: none;
                                border: 2px solid red;
                                margin: 0 0.5rem;
                            }
                        }
                    `);
                    document.adoptedStyleSheets = [...document.adoptedStyleSheets, reviewsStyles];
                }
            }
        }
        if(/^https:\/\/www\.allovoisins.com\/api\/messaging\/channel\/[0-9]+$/.test(responseURL.href)){
            let data;
            try { data = JSON.parse(responseText); } catch {}
            if(data)
                profileUrl = data['data']['relationships']['recipient']['user_url'];
        }
    }
});

(new MutationObserver(() => document
    .querySelectorAll(':has(> .avatar--s), [data-mtm-action="Profile"]')
    .forEach(element => {
        if(!profileElements.includes(element)){
            profileElements.push(element);
            element.addEventListener('click', event => {
                event.stopPropagation();
                window.open(profileUrl, '_blank');
            }, { capture: true });
        }
    })
)).observe(document.documentElement, { childList: true, subtree: true });

const styles = new CSSStyleSheet();
// language=CSS
styles.replaceSync(`
    .profile-completion-rate,
    .notification-badge,
    .profile-footer-actions,
    .conversion-premier,
    .bg-blue:has([href="espace-pro/ma-visibilite/voisin-a-la-une"]),
    article:has([src="https://static.allovoisins.com/assets/logos/part/small/logo-allovoisins-small.svg"]),
    article:has([src="https://static.allovoisins.com/images/blog/avatar-allovoisins.png"]),
    article:has(.fa-lock-alt),
    .link-preview:has([src="https://static.allovoisins.com/assets/pictos/allopro-badge-lock.svg"]),
    .apps_download_suggestions {
        display: none;
    }
    .__${APP_ID}__modal {
        z-index: 8;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        padding: 12.5vh 12.5vw;
        display: flex;
        flex-direction: column;
    }
    @media (hover: none) {
        .__${APP_ID}__modal {
            padding: 0;
        }
    }
    .__${APP_ID}__modal__iframe {
        flex: 1;
        border: 3px solid #8E44AD;
        border-bottom: none;
        background-color: #8E44AD;
    }
    .__${APP_ID}__modal__nav {
        background-color: #8E44AD;
        display: flex;
        align-items: center;
        justify-content: center;
        column-gap: 1rem;
    }
    .__${APP_ID}__modal__nav__button:hover {
        background-color: #9B59B6;
    }
`);
document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];
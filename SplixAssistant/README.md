[← Homepage](../README.md)

# Splix.IO Assistant

Enhance Splix.IO in-game experience.

| [Install](../../../../raw/branch/master/SplixAssistant/main.user.js) | [Install (alpha)](../../../../raw/branch/master/SplixAssistant/main.alpha.user.js) |
|----------------------------------------------------------------------|------------------------------------------------------------------------------------|

## DEPRECATION NOTICE

This project is very, very old, and unmaintained.

---

## Changelog

* `1.0.0` (????-??-??) • Initial release
* `1.0.1` (????-??-??) • Unknown update
* `1.2.0` (2017-06-21) • Unknown update
* `2.1.0-alpha` (2017-11-11) • Unknown update
/* ========================
SLIX.IO ASSISTANT by KaKi87
======================== */

// document.body.appendChild(kakiScript = document.createElement("script")); kakiScript.src = 'https://dl.kaki87.net/SplixAssistant_Alpha.js?n=' + Math.random();

/* ----- */

var scriptName = 'Splix Assistant v2.1 Alpha 11/11/17 © KaKi87';

var skSuffix = '| SK™';

var playground = {
	width: 598,
	height: 598,
	extremeX: {
		min: 1,
		max: 598
	},
	extremeY: {
		min: 1,
		max: 598
	}
}

var paused = null;
var ABK_enabled = true;
var intervals = [];

/* ----- */

console.clear();
console.log('%cLoading ' + scriptName, 'color: #E74C3C; font-weight: bold;');

if(window.location.href.indexOf('#') == -1){
	window.location.href += '#';
}

document.querySelector('#beginScreen').appendChild(about = document.createElement('a'));
about.id = 'about';
about.innerHTML = '<a href="https://www.kaki87.net/misc/splix_assistant/about.html" target="_blank" style="color: white;">About the script</a>';

var LD = 0; //  loaded dependancies

document.head.appendChild(kakiCSS = document.createElement('link'));
kakiCSS.rel = 'stylesheet';
kakiCSS.href = 'https://www.kaki87.net/misc/splix_assistant/SplixAssistant_Alpha.css?n=' + Math.random();
kakiCSS.onload = loaded(kakiCSS);

document.head.appendChild(splixMod = document.createElement('script'));
splixMod.src = 'https://www.kaki87.net/misc/splix_assistant/splix.js?n=' + Math.random();
// https://github.com/terjanq/terjanq.github.io/blob/master/scripts/splix.js
// splixMod.onload = loaded(splixMod);

function loaded(object){
	LD++;
	switch(object){
		case kakiCSS :
			console.log('Custom CSS loaded');
			break;
		case splixMod :
			console.log('Splix.JS Mod loaded');
			break;
	}
	if(LD == 2){
		console.log('%cReady !', 'color: #2ECC71; font-weight: bold;');
		init();
	}
}

var playing = function(){
	return playingAndReady == 1;
}

function init(){
	waitFor(playing, play);
	document.querySelector('#_servers').parentElement.appendChild(randomButton = document.createElement('span'));
	randomButton.id = 'randomButton';
	randomButton.innerText = 'Random';
	randomButton.onclick = randomServer;
	if(window.location.href.split('#')[1].length == 0){
		setTimeout(randomServer, 500);
	}
}

document.querySelector('#nameInput').onkeyup = function(){
	var initCursorPos = document.querySelector('#nameInput').selectionStart;
	if(!document.querySelector('#nameInput').value.endsWith(' ' + skSuffix)){
		document.querySelector('#nameInput').value += ' ' + skSuffix;
		document.querySelector('#nameInput').selectionEnd = initCursorPos;
	}
}

document.querySelector('#teamNameInput').value = 'SplixKrieg | SK™';
document.querySelector('#teamNameInput').disabled = true;

document.querySelector('#cornerLinks').appendChild(bestConfig = document.createElement('p'));
bestConfig.innerText = 'Best config';
bestConfig.id = 'bestConfig';
bestConfig.onclick = function(){
	if(bestConfig.className !== 'applied'){
		while(document.querySelector('#uglyText').innerText.split(':')[1].trim() !== 'on'){ document.querySelector('#uglyText').click(); }
		while(document.querySelector('#qualityText').innerText.split(':')[1].trim() !== 'high'){ document.querySelector('#qualityText').click(); }
		bestConfig.classList.add('applied');
		bestConfig.innerText = 'Best config applied.';
	}
}

document.body.appendChild(copyright = document.createElement('p'));
copyright.classList.add('display');
copyright.id = 'copyright';
copyright.innerHTML = '<span class="b">' + scriptName + '</span>';

setInterval(function(){
	document.querySelector('#copyright span').style.opacity = 0;
	setTimeout(function(){
		if(document.querySelector('#copyright span').className == 'b'){
			document.querySelector('#copyright span').innerText = document.querySelector('#nameInput').value.replace(' ' + skSuffix, '') + ' | SplixKrieg | #' + window.location.href.split('#')[1];
			document.querySelector('#copyright span').className = 'a';
		}
		else {
			document.querySelector('#copyright span').innerText = scriptName;
			document.querySelector('#copyright span').className = 'b';
		}
		document.querySelector('#copyright span').style.opacity = 1;
	}, 500);
}, 6000);

document.body.appendChild(toolbar = document.createElement('div'));
toolbar.classList.add('display');
toolbar.id = 'toolbar';
toolbar.innerHTML = '<span class="x">[x]</span> <span class="y">[y]</span> | <span id="pause">PAUSE</span> (*)';
toolbar.querySelector('#pause').onclick = togglePause;

function randomServer(){
	document.querySelector('#_servers').selectedIndex = getRandomInt(1, document.querySelector('#_servers').length - 1);
	window.location.replace(document.querySelector('#_servers').value);
}

function play(){
	var Playing = setInterval(function(){
		if(playingAndReady){
			displayLocation();
			if(grid.style.display == 'none')
				grid.style.display = 'initial';
			if(toolbar.style.display == 'none')
				toolbar.style.display = 'initial';
		}
		else {
			clearInterval(Playing);
			clear();
			waitFor(playing, play);
			lastActions.length = 0;
			grid.style.display = 'none';
			document.querySelectorAll('.x, .y').forEach(function(el){ el.innerText = '[' + el.className + ']'; });
			toolbar.querySelectorAll('.nearBorder').forEach(function(el){ el.classList.remove('nearBorder'); });
		}
	}, 0);
}

function displayLocation(){
	toolbar.querySelector('.x').innerText = parseInt(myPlayer.pos[0]);
	toolbar.querySelector('.y').innerText = parseInt(myPlayer.pos[1]);
	if(myPlayer.pos[0] <= playground.extremeX.min + 30 || myPlayer.pos[0] >= playground.extremeX.max - 30)
		toolbar.querySelector('.x').classList.add('nearBorder');
	else
		toolbar.querySelector('.x').classList.remove('nearBorder');
	if(myPlayer.pos[1] <= playground.extremeY.min + 30 || myPlayer.pos[1] >= playground.extremeY.max - 30)
		toolbar.querySelector('.y').classList.add('nearBorder');
	else
		toolbar.querySelector('.y').classList.remove('nearBorder');
}

function togglePause(){
	paused = paused ? false : true;
	document.querySelector('#pause').style.color = paused ? '#27AE60' : '#C0392B';
}

var lastActions = [];

var dir = {
	up: 3,
	down: 1,
	left: 2,
	right: 0,
	go: function(direction, callback){
		if(myPlayer.dir !== direction){
			if((direction == dir.up && myPlayer.pos[1] == playground.extremeY.min)
			|| (direction == dir.down && myPlayer.pos[1] == playground.extremeY.max)
			|| (direction == dir.left && myPlayer.pos[0] == playground.extremeX.min)
			|| (direction == dir.right && myPlayer.pos[0] == playground.extremeX.max))
			{
				return;
			}
			if(sendDir(direction)){
				var initPos = myPlayer.pos;
				console.log(dir.valueToString(direction)); // debug
				switch(direction){
					case dir.up :
						var condition = function(){
							return myPlayer.pos[1] < initPos[1];
						}
						if(lastActions[lastActions.length - 1] !== direction){
							lastActions[lastActions.length] = direction;
						}
						break;
					case dir.down :
						var condition = function(){
							return myPlayer.pos[1] > initPos[1];
						}
						if(lastActions[lastActions.length - 1] !== direction){
							lastActions[lastActions.length] = direction;
						}
						break;
					case dir.left :
						var condition = function(){
							return myPlayer.pos[0] < initPos[0];
						}
						if(lastActions[lastActions.length - 1] !== direction){
							lastActions[lastActions.length] = direction;
						}
						break;
					case dir.right :
						var condition = function(){
							return myPlayer.pos[0] > initPos[0];
						}
						if(lastActions[lastActions.length - 1] !== direction){
							lastActions[lastActions.length] = direction;
						}
						break;
				}
				if(callback !== undefined){
					waitFor(condition, function(){
						callback();
					});
				}
			}
			else {
				if(direction == dir.up || direction == dir.down){
					switch(lastActions[lastActions.length - 2]){
						case dir.left :
							dir.go(dir.right, function(){
								dir.go(direction, callback);
							});
							break;
						case dir.right :
							dir.go(dir.left, function(){
								dir.go(direction, callback);
							});
							break;
						// case default
					}
				}
				else if(direction == dir.left || direction == dir.right){
					switch(lastActions[lastActions.length - 2]){
						case dir.up :
							dir.go(dir.down, function(){
								dir.go(direction, callback);
							});
							break;
						case dir.down :
							dir.go(dir.up, function(){
								dir.go(direction, callback);
							});
							break;
						// case default
					}
				}
			}
		}
	},
	valueToString: function(direction){
		switch(direction){
			case dir.up :
				return 'up';
			case dir.down :
				return 'down';
			case dir.left :
				return 'left';
			case dir.right :
				return 'right';
		}
	}
}

function suicide(){
	switch(myPlayer.dir){
		case dir.up :
			dir.go(dir.right, function(){
				dir.go(dir.down, function(){
					dir.go(dir.left);
				});
			});
			break;
		case dir.down :
			dir.go(dir.left, function(){
				dir.go(dir.up, function(){
					dir.go(dir.right);
				});
			});
			break;
		case dir.left :
			dir.go(dir.up, function(){
				dir.go(dir.right, function(){
					dir.go(dir.down);
				});
			});
			break;
		case dir.right :
			dir.go(dir.down, function(){
				dir.go(dir.left, function(){
					dir.go(dir.up);
				});
			});
			break;
	}
}

function findPlayer(playerName){
	if(typeof(playerName) == 'string'){
		for(var i = 0; i < players.length; i++){
			if(players[i].name == playerName)
				return players[i];
		}
	}
}

function listPlayers(){
	if(players.length > 1){
		if(players.length > 2)
			console.log(players.length - 1 + ' players in local area');
		else
			console.log(players.length - 1 + ' player in local area');
		for(var i = 1; i < players.length; i++){
			console.log(players[i].name + ' (' + playerDistance(players[i].name) + ')');
		}
	}
	else
		console.log('You\'re alone.');
}

function playerDistance(playerName){
	if(typeof(playerName) == 'string'){
		// return Math.abs(Math.round(myPlayer.pos[0] - findPlayer(playerName).pos[0] + myPlayer.pos[1] - findPlayer(playerName).pos[1]));
		var a = myPlayer.pos[0] - findPlayer(playerName).pos[0];
		var b = myPlayer.pos[1] - findPlayer(playerName).pos[1]
		var c = Math.round(Math.sqrt(a*a+b*b));
		return c;
	}
}

function nearestPlayer(){
	if(players.length > 1){
		var playersArray = [], distanceArray = [], nearestPlayer = {};
		for(var i = 1; i < players.length; i++){
			playersArray[i-1] = players[i].name;
			distanceArray[i-1] = playerDistance(players[i].name);
		}
		nearestPlayer.distance = Math.min.apply(Math, distanceArray);
		nearestPlayer.name = playersArray[distanceArray.indexOf(nearestPlayer.distance)];
		return nearestPlayer;
	}
	else
		return false;
}

var antiBorderKill = setInterval(function(){
	if(playingAndReady && ABK_enabled){
		if((myPlayer.pos[0] < playground.extremeX.min || myPlayer.pos[0] > playground.extremeX.max) // Borders left/right
		&& (myPlayer.dir == dir.left || myPlayer.dir == dir.right)){
			switch(lastActions[lastActions.length - 2]){
				case dir.up :
					dir.go(dir.down);
					break;
				case dir.down :
					dir.go(dir.up);
					break;
				default :
					dir.go(dir.down);
					break;
			}
		}
		else if((myPlayer.pos[1] < playground.extremeY.min || myPlayer.pos[1] > playground.extremeY.max) // Borders top/bottom
			&& (myPlayer.dir == dir.up || myPlayer.dir == dir.down)){
			switch(lastActions[lastActions.length - 2]){
				case dir.left :
					dir.go(dir.right);
					break;
				case dir.right :
					dir.go(dir.left);
					break;
				default :
					dir.go(dir.right);
					break;
			}
		}
	}
}, 0);

(function improveMinimap(){
	document.body.appendChild(grid = document.createElement('div'));
	grid.id = 'grid';
	grid.style.display = 'none';
	grid.appendChild(gridImg = document.createElement('img'));
	gridImg.src = 'https://www.kaki87.net/misc/splix_assistant/grid-letters-min.png';
	waitFor(playing, function(){
		document.querySelector('#miniMap').style.transform = 'scale(1)';
		while(document.querySelector('#miniMap').getBoundingClientRect().width < window.innerWidth * 0.12){
			document.querySelector('#miniMap').style.transform = 'scale(' + (parseFloat(document.querySelector('#miniMap').style.transform.match('\(([0-9.]+)\)')[0]) + 0.01) + ')';
		}
		gridImg.style.width = gridImg.style.height = (document.querySelector('#miniMap').getBoundingClientRect().width + 10) + 'px';
	});
})(0);

function getRandomInt(min, max){
	return Math.floor(Math.random() * (max - min + 1) + min);
}

/* ===== KEYBOARD INTERACTIONS ===== */

var keyboardMap = [
	'', // [0]
	'', // [1]
	'', // [2]
	'CANCEL', // [3]
	'', // [4]
	'', // [5]
	'HELP', // [6]
	'', // [7]
	'BACK_SPACE', // [8]
	'TAB', // [9]
	'', // [10]
	'', // [11]
	'CLEAR', // [12]
	'ENTER', // [13]
	'ENTER_SPECIAL', // [14]
	'', // [15]
	'SHIFT', // [16]
	'CONTROL', // [17]
	'ALT', // [18]
	'PAUSE', // [19]
	'CAPS_LOCK', // [20]
	'KANA', // [21]
	'EISU', // [22]
	'JUNJA', // [23]
	'FINAL', // [24]
	'HANJA', // [25]
	'', // [26]
	'ESCAPE', // [27]
	'CONVERT', // [28]
	'NONCONVERT', // [29]
	'ACCEPT', // [30]
	'MODECHANGE', // [31]
	'SPACE', // [32]
	'PAGE_UP', // [33]
	'PAGE_DOWN', // [34]
	'END', // [35]
	'HOME', // [36]
	'LEFT', // [37]
	'UP', // [38]
	'RIGHT', // [39]
	'DOWN', // [40]
	'SELECT', // [41]
	'PRINT', // [42]
	'EXECUTE', // [43]
	'PRINTSCREEN', // [44]
	'INSERT', // [45]
	'DELETE', // [46]
	'', // [47]
	'0', // [48]
	'1', // [49]
	'2', // [50]
	'3', // [51]
	'4', // [52]
	'5', // [53]
	'6', // [54]
	'7', // [55]
	'8', // [56]
	'9', // [57]
	'COLON', // [58]
	'SEMICOLON', // [59]
	'LESS_THAN', // [60]
	'EQUALS', // [61]
	'GREATER_THAN', // [62]
	'QUESTION_MARK', // [63]
	'AT', // [64]
	'A', // [65]
	'B', // [66]
	'C', // [67]
	'D', // [68]
	'E', // [69]
	'F', // [70]
	'G', // [71]
	'H', // [72]
	'I', // [73]
	'J', // [74]
	'K', // [75]
	'L', // [76]
	'M', // [77]
	'N', // [78]
	'O', // [79]
	'P', // [80]
	'Q', // [81]
	'R', // [82]
	'S', // [83]
	'T', // [84]
	'U', // [85]
	'V', // [86]
	'W', // [87]
	'X', // [88]
	'Y', // [89]
	'Z', // [90]
	'OS_KEY', // [91] Windows Key (Windows) or Command Key (Mac)
	'', // [92]
	'CONTEXT_MENU', // [93]
	'', // [94]
	'SLEEP', // [95]
	'NUMPAD0', // [96]
	'NUMPAD1', // [97]
	'NUMPAD2', // [98]
	'NUMPAD3', // [99]
	'NUMPAD4', // [100]
	'NUMPAD5', // [101]
	'NUMPAD6', // [102]
	'NUMPAD7', // [103]
	'NUMPAD8', // [104]
	'NUMPAD9', // [105]
	'MULTIPLY', // [106]
	'ADD', // [107]
	'SEPARATOR', // [108]
	'SUBTRACT', // [109]
	'DECIMAL', // [110]
	'DIVIDE', // [111]
	'F1', // [112]
	'F2', // [113]
	'F3', // [114]
	'F4', // [115]
	'F5', // [116]
	'F6', // [117]
	'F7', // [118]
	'F8', // [119]
	'F9', // [120]
	'F10', // [121]
	'F11', // [122]
	'F12', // [123]
	'F13', // [124]
	'F14', // [125]
	'F15', // [126]
	'F16', // [127]
	'F17', // [128]
	'F18', // [129]
	'F19', // [130]
	'F20', // [131]
	'F21', // [132]
	'F22', // [133]
	'F23', // [134]
	'F24', // [135]
	'', // [136]
	'', // [137]
	'', // [138]
	'', // [139]
	'', // [140]
	'', // [141]
	'', // [142]
	'', // [143]
	'NUM_LOCK', // [144]
	'SCROLL_LOCK', // [145]
	'WIN_OEM_FJ_JISHO', // [146]
	'WIN_OEM_FJ_MASSHOU', // [147]
	'WIN_OEM_FJ_TOUROKU', // [148]
	'WIN_OEM_FJ_LOYA', // [149]
	'WIN_OEM_FJ_ROYA', // [150]
	'', // [151]
	'', // [152]
	'', // [153]
	'', // [154]
	'', // [155]
	'', // [156]
	'', // [157]
	'', // [158]
	'', // [159]
	'CIRCUMFLEX', // [160]
	'EXCLAMATION', // [161]
	'DOUBLE_QUOTE', // [162]
	'HASH', // [163]
	'DOLLAR', // [164]
	'PERCENT', // [165]
	'AMPERSAND', // [166]
	'UNDERSCORE', // [167]
	'OPEN_PAREN', // [168]
	'CLOSE_PAREN', // [169]
	'ASTERISK', // [170]
	'PLUS', // [171]
	'PIPE', // [172]
	'HYPHEN_MINUS', // [173]
	'OPEN_CURLY_BRACKET', // [174]
	'CLOSE_CURLY_BRACKET', // [175]
	'TILDE', // [176]
	'', // [177]
	'', // [178]
	'', // [179]
	'', // [180]
	'VOLUME_MUTE', // [181]
	'VOLUME_DOWN', // [182]
	'VOLUME_UP', // [183]
	'', // [184]
	'', // [185]
	'SEMICOLON', // [186]
	'EQUALS', // [187]
	'COMMA', // [188]
	'MINUS', // [189]
	'PERIOD', // [190]
	'SLASH', // [191]
	'BACK_QUOTE', // [192]
	'', // [193]
	'', // [194]
	'', // [195]
	'', // [196]
	'', // [197]
	'', // [198]
	'', // [199]
	'', // [200]
	'', // [201]
	'', // [202]
	'', // [203]
	'', // [204]
	'', // [205]
	'', // [206]
	'', // [207]
	'', // [208]
	'', // [209]
	'', // [210]
	'', // [211]
	'', // [212]
	'', // [213]
	'', // [214]
	'', // [215]
	'', // [216]
	'', // [217]
	'', // [218]
	'OPEN_BRACKET', // [219]
	'BACK_SLASH', // [220]
	'CLOSE_BRACKET', // [221]
	'QUOTE', // [222]
	'', // [223]
	'META', // [224]
	'ALTGR', // [225]
	'', // [226]
	'WIN_ICO_HELP', // [227]
	'WIN_ICO_00', // [228]
	'', // [229]
	'WIN_ICO_CLEAR', // [230]
	'', // [231]
	'', // [232]
	'WIN_OEM_RESET', // [233]
	'WIN_OEM_JUMP', // [234]
	'WIN_OEM_PA1', // [235]
	'WIN_OEM_PA2', // [236]
	'WIN_OEM_PA3', // [237]
	'WIN_OEM_WSCTRL', // [238]
	'WIN_OEM_CUSEL', // [239]
	'WIN_OEM_ATTN', // [240]
	'WIN_OEM_FINISH', // [241]
	'WIN_OEM_COPY', // [242]
	'WIN_OEM_AUTO', // [243]
	'WIN_OEM_ENLW', // [244]
	'WIN_OEM_BACKTAB', // [245]
	'ATTN', // [246]
	'CRSEL', // [247]
	'EXSEL', // [248]
	'EREOF', // [249]
	'PLAY', // [250]
	'ZOOM', // [251]
	'', // [252]
	'PA1', // [253]
	'WIN_OEM_CLEAR', // [254]
	'' // [255]
];

document.body.onkeydown = function(){
	if(playingAndReady){
		switch(keyboardMap[event.keyCode]){
			case 'NUMPAD8' :
				dir.go(dir.up);
				break;
			case 'NUMPAD6' :
				dir.go(dir.right);
				break;
			case 'NUMPAD5' :
				dir.go(dir.down);
				break;
			case 'NUMPAD2' :
				dir.go(dir.down);
				break;
			case 'NUMPAD4' :
				dir.go(dir.left);
				break;
			case 'NUMPAD0' :
				suicide();
				break;
			case 'MULTIPLY' :
				togglePause();
				break;
		}
	}
	else {
		switch(keyboardMap[event.keyCode]){
			case 'MULTIPLY' :
				if(event.target.tagName !== 'INPUT')
					togglePause();
				break;
			case 'ENTER' :
				document.querySelector('#joinButton').click();
				break;
		}
	}
}

function waitFor(condition, callback){
	var i = intervals.length;
	intervals[i] = setInterval(function(){
		if(condition() == 1){
			clearInterval(intervals[i]);
			intervals[i] == null;
			console.log(intervals[i]);
			callback();
		}
	}, 0);
}

function clear(){
	var i = 0;
	intervals.forEach(function(item){
		if(item !== null){
			console.log(item); // debug
			clearInterval(item);
			i++;
		}
	});
	intervals.length = 0;
}
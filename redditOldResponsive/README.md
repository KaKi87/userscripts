[← Homepage](../README.md)

# Responsive design for Reddit Old

Mobile-friendly design for [old.reddit.com](https://old.reddit.com).

Since mid-2024, [chat.reddit.com](https://chat.reddit.com) prevents mobile use, but responsive design still works in combination with the [User-Agent Switcher and Manager](https://chromewebstore.google.com/detail/user-agent-switcher-and-m/bhchdcejhohfmigjafbampogmaanbfkg) extension.

| [Install](../../../../raw/branch/master/redditOldResponsive/main.user.js) |
|---------------------------------------------------------------------------|

| ![](./screenshot1.png) | ![](./screenshot2.png) |
|------------------------|------------------------|
| ![](./screenshot3.png) | ![](./screenshot4.png) |
| ![](./screenshot5.png) | ![](./screenshot6.png) |

Related projects :

- [Mobile RES Reddit — UserStyles.world](https://userstyles.world/style/18076/mobile-res-reddit)
[← Homepage](../README.md)

# Cloudflare detector for Qwant search *[Why?](https://framagit.org/dCF/deCloudflare/-/blob/master/readme/en.md)*

Tag Qwant search results pointing to Cloudflare-protected websites.

| [Install](../../../../raw/branch/master/qwantCloudflareDetector/main.user.js) |
|-------------------------------------------------------------------------------|

## Changelog

* `0.1.0` (2023-04-22) • Initial release
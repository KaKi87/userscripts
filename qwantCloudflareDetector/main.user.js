// ==UserScript==
// @match       https://www.qwant.com/
// @name        Cloudflare detector for Qwant
// @description Tag Qwant results pointing to Cloudflare-protected websites
// @grant       GM.xmlHttpRequest
// @version     0.1.0
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/qwantCloudflareDetector
// @require     https://cdn.jsdelivr.net/npm/@trim21/gm-fetch
// @require     https://git.kaki87.net/KaKi87/userscripts/raw/commit/9233234ec6bffeaa2ab663e4f4144a3615bf93a6/_lib/isCloudflareDomain.js
// ==/UserScript==

const resultElements = [];
new MutationObserver(async () => {
    for(const resultElement of document.querySelectorAll('[data-testid="webResult"]')){
        if(!resultElements.includes(resultElement)){
            resultElements.push(resultElement);
            const domain = new URL(resultElement.getAttribute('domain')).host;
            if(await isCloudflareDomain(domain, GM_fetch)){
                const cloudflareIndicatorElement = document.createElement('span');
                cloudflareIndicatorElement.textContent = 'Cloudflare';
                cloudflareIndicatorElement.setAttribute(
                    'style',
                    [
                        'padding: 0 0.25rem',
                        'background-color: #C0392B',
                        'color: #ECF0F1',
                        'font-size: 12px',
                        'border-radius: 3px'
                    ].join(';')
                );
                resultElement.querySelector(':nth-child(2)').insertAdjacentElement('afterbegin', cloudflareIndicatorElement);
            }
        }
    }
}).observe(document.body, { childList: true, subtree: true });
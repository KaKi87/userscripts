[← Homepage](../README.md)

# RARBG threat defence bypasser

Automatically fill & submit captcha.

| [Install](../../../../raw/branch/master/rarbgThreatDefenceBypasser/main.user.js) |
|----------------------------------------------------------------------------------|

## DEPRECATION NOTICE

Not having encountered any captcha on RARBG for a while, this script seem to have outlived its usefulness.

---

## Changelog

* `1.0.0` (2020-11-15) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/0050f1870bfeb819786b2696da5220d3fc90638d/rarbgThreatDefenceBypasser)
* `1.0.1` (2020-12-21) • [Remove DOMContentLoaded listener (fix Firefox support)](https://git.kaki87.net/KaKi87/userscripts/src/commit/20b26d14321389025da8ca57c9006d66988437da/rarbgThreatDefenceBypasser)
* `1.1.0` (2020-12-23) • [Support RARBG mirrors](https://git.kaki87.net/KaKi87/userscripts/src/commit/0efbfeebe7e0a585eebcbab8651c7afbaefbef22/rarbgThreatDefenceBypasser)
[← Homepage](../README.md)

# Reddit Force Local Search

Force search in subreddit by default.

| [Install](../../../../raw/branch/master/RedditForceLocalSearch/main.user.js) |
|------------------------------------------------------------------------------|

## DEPRECATION NOTICE

This project is no longer working nor maintained.

I would like to recommend you to use [old.reddit.com](https://old.reddit.com) instead, which is still bloatware-free for now.

Thanks for understanding.

---

More info :

- [r/help - Can no longer search within subreddit by default](https://www.reddit.com/r/help/comments/f42mn3/can_no_longer_search_within_subreddit_by_default/)
- [r/help - Searching should default to within current subreddit, not reddit-wide](https://www.reddit.com/r/help/comments/f9rsvh/searching_should_default_to_within_current/)

![](./screenshot.png)

## Changelog

* `1.0.0` (2020-04-01) • [Initial release](https://git.kaki87.net/KaKi87/userscripts/src/commit/3330f9a72505d8d9b128ca6c990b20cc1848f524/RedditForceLocalSearch)
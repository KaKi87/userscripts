[← Homepage](../README.md)

# Enhancements for GitHub Actions

Enhanced GitHub Actions with notification updates & progress bar.

| [Install](../../../../raw/branch/master/GitHubActionsEnhanced/main.user.js) |
|-----------------------------------------------------------------------------|
// ==UserScript==
// @match       https://alternativeto.net/software/*
// @name        Enhancements for AlternativeTo
// @description Filtering enhancements for AlternativeTo.net
// @grant       none
// @version     0.1.0
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/alternativetoEnhanced
// ==/UserScript==

const listElement = document.querySelector('[data-testid="alternative-list"] > ol');
if(listElement){
    document.head.appendChild(
        Object.assign(
            document.createElement('link'),
            {
                'rel': 'stylesheet',
                'href': 'https://cdn.jsdelivr.net/npm/fork-awesome@1.2.0/css/fork-awesome.min.css'
            }
        )
    );
    const
        APP_ID = 'alternativetoEnhanced',
        features = new Set();
    {
        const init = () => {
            for(const itemElement of document.querySelectorAll('[id^="item-"]')){
                const itemFeatures = [...itemElement.querySelectorAll('.badges span, [href^="/feature/"]')].map(element => element.textContent);
                itemElement.setAttribute(
                    `data-${APP_ID}-features`,
                    itemFeatures.map(feature => feature.replace(/\s/g, '')).join(' ')
                );
                for(const feature of itemFeatures)
                    features.add(feature);
            }
        };
        new MutationObserver(init).observe(listElement, { childList: true });
        init();
    }
    {
        const staticStyleElement = new CSSStyleSheet();
        staticStyleElement.replace(`
            .__${APP_ID}__toggleFilters:not(.__${APP_ID}__toggleFilters--active) .fa-times,
            .__${APP_ID}__toggleFilters--active .fa-filter,
            .__${APP_ID}__filters:not(.__${APP_ID}__filters--active) {
                display: none;
            }
            .__${APP_ID}__toggleFilters,
            .__${APP_ID}__filters {
                z-index: 1;
                position: fixed;
                bottom: 0;
                right: 0;
            }
            .__${APP_ID}__toggleFilters {
                margin: 0 1rem 1rem 0;
                width: 2.5rem;
                height: 2.5rem;
                background-color: var(--popBrand);
                border-radius: 50%;
                border: none;
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 1.5rem;
                color: var(--mainFg);
            }
            .__${APP_ID}__filters {
                margin: 0 1rem 4.5rem 0;
                padding: 0.5rem;
                width: 25rem;
                height: 50vh;
                background-color: var(--mainBg);
                border-radius: 3px;
                overflow-y: auto;
                box-shadow: var(--mainFg) 0 0 9px 2px;
            }
            .__${APP_ID}__filters--active {
                display: flex;
                flex-direction: column;
                row-gap: 0.5rem;
            }
            .__${APP_ID}__filters__filter {
                display: flex;
                align-items: center;
                column-gap: 0.5rem;
            }
            .__${APP_ID}__filters__filter__label {
                flex: 1;
            }
            .__${APP_ID}__filters__filter__option {
                width: 1.5rem;
                height: 1.5rem;
                display: flex;
                align-items: center;
                justify-content: center;
                border: 1px solid var(--color);
                color: var(--color);
                background-color: transparent;
            }
            .__${APP_ID}__filters__filter__option--active {
                color: white;
                background-color: var(--color);
            }
            .__${APP_ID}__filters__filter__option--exclude {
                --color: var(--danger);
            }
            .__${APP_ID}__filters__filter__option--default {
                --color: var(--mainFg);
            }
            .__${APP_ID}__filters__filter__option--include {
                --color: var(--positiveGreener);
            }
            @media screen and (max-width: 30rem) {
                .__${APP_ID}__filters {
                    width: calc(100vw - 2rem);
                }
            }
        `);
        document.adoptedStyleSheets = [...document.adoptedStyleSheets, staticStyleElement];
    }
    {
        const
            toggleFiltersElement = document.createElement('button'),
            filtersElement = document.createElement('ul'),
            featureFilterElement = {},
            excludedFeatures = new Set(),
            includedFeatures = new Set(),
            filtersStyleElement = new CSSStyleSheet(),
            refreshFiltersStyleElement = () => filtersStyleElement.replace([
                ...[...excludedFeatures].map(feature => `[data-${APP_ID}-features~="${feature.replace(/\s/g, '')}"] { display: none }`),
                ...[...includedFeatures].map(feature => `[data-${APP_ID}-features]:not([data-${APP_ID}-features~="${feature.replace(/\s/g, '')}"]) { display: none }`)
            ].join('')),
            refreshFiltersElement = () => {
                for(const feature of features){
                    if(!featureFilterElement[feature]){
                        featureFilterElement[feature] = document.createElement('li');
                        const
                            featureFilterLabelElement = document.createElement('p'),
                            featureFilterExcludeElement = document.createElement('button'),
                            featureFilterDefaultElement = document.createElement('button'),
                            featureFilterIncludeElement = document.createElement('button');
                        featureFilterElement[feature].classList.add(`__${APP_ID}__filters__filter`);
                        featureFilterElement[feature].setAttribute(`data-${APP_ID}-feature`, feature);
                        filtersElement.appendChild(featureFilterElement[feature]);
                        featureFilterLabelElement.classList.add(`__${APP_ID}__filters__filter__label`);
                        featureFilterLabelElement.textContent = feature;
                        featureFilterElement[feature].appendChild(featureFilterLabelElement);
                        [
                            featureFilterExcludeElement,
                            featureFilterDefaultElement,
                            featureFilterIncludeElement
                        ].forEach(element => {
                            element.classList.add(`__${APP_ID}__filters__filter__option`);
                            featureFilterElement[feature].appendChild(element);
                        });
                        featureFilterExcludeElement.classList.add(`__${APP_ID}__filters__filter__option--exclude`);
                        featureFilterExcludeElement.textContent = '✘';
                        featureFilterExcludeElement.addEventListener('click', () => {
                            excludedFeatures.add(feature);
                            includedFeatures.delete(feature);
                            refreshFiltersStyleElement();
                            featureFilterExcludeElement.classList.add(`__${APP_ID}__filters__filter__option--active`);
                            featureFilterDefaultElement.classList.remove(`__${APP_ID}__filters__filter__option--active`);
                            featureFilterIncludeElement.classList.remove(`__${APP_ID}__filters__filter__option--active`);
                        });
                        featureFilterDefaultElement.classList.add(`__${APP_ID}__filters__filter__option--default`);
                        featureFilterDefaultElement.classList.add(`__${APP_ID}__filters__filter__option--active`);
                        featureFilterDefaultElement.textContent = '/';
                        featureFilterDefaultElement.addEventListener('click', () => {
                            excludedFeatures.delete(feature);
                            includedFeatures.delete(feature);
                            refreshFiltersStyleElement();
                            featureFilterDefaultElement.classList.add(`__${APP_ID}__filters__filter__option--active`);
                            featureFilterExcludeElement.classList.remove(`__${APP_ID}__filters__filter__option--active`);
                            featureFilterIncludeElement.classList.remove(`__${APP_ID}__filters__filter__option--active`);
                        });
                        featureFilterIncludeElement.classList.add(`__${APP_ID}__filters__filter__option--include`);
                        featureFilterIncludeElement.textContent = '✔';
                        featureFilterIncludeElement.addEventListener('click', () => {
                            excludedFeatures.delete(feature);
                            includedFeatures.add(feature);
                            refreshFiltersStyleElement();
                            featureFilterIncludeElement.classList.add(`__${APP_ID}__filters__filter__option--active`);
                            featureFilterExcludeElement.classList.remove(`__${APP_ID}__filters__filter__option--active`);
                            featureFilterDefaultElement.classList.remove(`__${APP_ID}__filters__filter__option--active`);
                        });
                    }
                }
            };
        toggleFiltersElement.classList.add(`__${APP_ID}__toggleFilters`);
        toggleFiltersElement.innerHTML = '<i class="fa fa-filter"></i><i class="fa fa-times"></i>';
        toggleFiltersElement.addEventListener('click', () => {
            refreshFiltersElement();
            toggleFiltersElement.classList.toggle(`__${APP_ID}__toggleFilters--active`);
            filtersElement.classList.toggle(`__${APP_ID}__filters--active`);
        });
        document.body.appendChild(toggleFiltersElement);
        filtersElement.classList.add(`__${APP_ID}__filters`);
        document.body.appendChild(filtersElement);
        document.adoptedStyleSheets = [...document.adoptedStyleSheets, filtersStyleElement];
    }
}
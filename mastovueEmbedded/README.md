[← Homepage](../README.md)

# MastoVue embedded

[MastoVue](https://mastovue.glitch.me/#/) embedded into [Mastodon](https://joinmastodon.org/) (v3.x & v4.x supported).

| [Install](../../../../raw/branch/master/mastovueEmbedded/main.user.js) |
|------------------------------------------------------------------------|

Click on a profile or post to copy its URL, then paste into search to interact with it.

| ![](./screenshot.png) | ![](./screenshot2.png) |
|-----------------------|------------------------|

## DEPRECATION NOTICE

This project is now deprecated in favor of [MastoVue++](https://git.kaki87.net/KaKi87/mastovuepp).
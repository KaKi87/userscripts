[← Homepage](../README.md)

# YGGTorrent Infinite Session

*Stay signed in for real*

| [Install](../../../../raw/branch/master/YGGTorrentInfiniteSession/main.user.js) |
|---------------------------------------------------------------------------------|

## Introduction

Le cookie de session du site Internet du tracker torrent YGG a une durée de vie de seulement *2 heures*.

Cette durée étant extrêmement *faible*, et aucune option d'augmentation de ce délai n'étant disponible, j'ai créé la mienne.

## Fonctionnement

Ce script fonctionne de façon tout à fait **transparente** : il *mémorise* vos informations de connexion lorsque vous vous connectez, et les *oublie* lorsque vous vous déconnectez.

Entre temps, si le cookie de session expire, le script vous reconnectera automatiquement. *(sa raison d'être)*

Le code source est documenté et formaté du mieux possible pour assurer sa lisibilité et permettre son audit.

## Sécurité

### Identifiants

Vos identifiants ne sont accessibles que dans le cadre du script.

Cependant, vous devez considérer que la mémorisation par le script est équivalente à la **mémorisation par le navigateur** en termes de sécurité, c'est-à-dire qu'il est *facilement* possible de les récupérer par un accès physique à votre ordinateur ou par un virus.

### Navigation

Le script utilise une expression régulière afin de détecter intelligemment le site YGGTorrent.

Cette détection est positive si le protocole est HTTPS *(et non HTTP)* et que le nom de domaine correspond exactement à `ygg` ou `yggtorrent`.

Par exemple, ceux-ci fonctionneront :

- `https://ygg.is`
- `https://yggtorrent.is`
- `https://yggtorrent.pe`
- `https://yggtorrent.ws`
- `https://ww2.yggtorrent.ws`

Mais ceux-ci ne fonctionneront pas :

- `http://ygg.is`
- `http://yggtorrent.is`
- `https://yggfake.com`
- `https://fakeygg.com`
- `https://ygg.fake.com`

Bien que la plupart des fake ne fassent pas autant d'effort, certains pourraient, et il est de votre responsabilité *(et non de la mienne)* de ne pas les fréquenter, auquel cas la connexion automatique pourrait se déclencher et vos identifiants seraient alors transmis à des pirates.

Si cela devait toutefois arriver, pensez à vous rendre sur le vrai site, renouveler votre passkey et changer votre mot de passe.

Aussi, n'utilisez jamais un mot de passe sur plusieurs sites.

*Il est possiblement envisagé dans le futur de n'activer la connexion automatique que sur les domaines où l'utilisateur s'est déjà manuellement connecté.*

## Cas d'utilisation

Ce script est (presque) inutile si vous mémorisez vos mots de passe dans votre navigateur.
Néanmoins, votre navigateur ne saura pas automatiser la connexion, et c'est le rôle de ce script.

De plus, utilisant un gestionnaire de mots de passe au quotidien, et mes mots de passe étant donc générés, le copier/coller plusieurs fois par jour peut s'avérer ennuyant.

## Licence

Ce script est distribué sous la licence [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html).

## Maintenance

Ce script sera maintenu aussi longtemps qu'il aura une raison d'être, c'est-à-dire que le besoin d'automatiser la reconnexion au site en raison de l'expiration trop rapide de la session, sera un problème.

## Support

Ping *@KaKi87#2368* sur le serveur Discord [We Torrent](https://discord.gg/QkJzWSD).

## Notes de versions

* `1.0.0` (01/01/2020) • [Publication initiale](https://git.kaki87.net/KaKi87/userscripts/src/commit/3330f9a72505d8d9b128ca6c990b20cc1848f524/YGGTorrentInfiniteSession)
* `1.1.0` (15/11/2020) • [(Fonctionnalité diverse) Masquage des publicités natives](https://git.kaki87.net/KaKi87/userscripts/src/commit/13446fa389b07a6738a8c8dd7397f0c14b191f66/YGGTorrentInfiniteSession)
* `1.1.0` (22/01/2023) • [(Fonctionnalité diverse) Masquage des publicités natives](https://git.kaki87.net/KaKi87/userscripts/src/commit/cb7b10adb680ae0be9233aa335804b5bf8439e4d/YGGTorrentInfiniteSession)
* `1.3.0` (12/04/2023) • [(Fonctionnalité diverse) Validation du formulaire de recherche avancée en appuyant sur `Entrer` sur mobile](https://git.kaki87.net/KaKi87/userscripts/src/commit/a78b3346355a6a4b3099bad0d17b65de21302dd9/YGGTorrentInfiniteSession)
* `1.3.1` (30/06/2023) • [(Fonctionnalité diverse) Masquage des publicités natives](https://git.kaki87.net/KaKi87/userscripts/src/commit/bf7b0728b501ce4ec87d46fb341d4946b7084030/YGGTorrentInfiniteSession)
* `1.4.0` (13/07/2023) • [Mémorisation de la connexion au téléchargement de torrent](https://git.kaki87.net/KaKi87/userscripts/src/commit/b951873fc98fa4ffd1f1f8666d0937dec3857031/YGGTorrentInfiniteSession)
* `1.5.0` (02/11/2023) • Amélioration du responsive design

---

[Remerciement spécial à Hiro sur Wareziens.net pour la petite pub.](https://www.wareziens.net/forum/topic-43322-topic-unique-yggtorrent-new-info-pubs-page-1-page-18.html#p537181)
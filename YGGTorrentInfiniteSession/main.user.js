// ==UserScript==
// @match       https://*.yggtorrent.*/*
// @match       https://ygg.*/*
// @name        YGGTorrent infinite session
// @description Stay signed in for real
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.deleteValue
// @version     1.5.0
// @author      KaKi87
// @license     GPL-3.0-or-later
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/YGGTorrentInfiniteSession
// ==/UserScript==

/*

 Copyright (C) 2020 - KaKi87

 This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

 */

/*
 * [ Function ] Get logout link
 *
 * NOTE : link not in DOM while logged out
 *
 */

const getLogout = () => document.querySelector('[href$="/user/logout"]');

/*
 * [ Event handler ] Delete credentials on logout
 *
 */

const createLogoutHandler = () => {

    const logout = getLogout();

    if(!logout) return;

    logout.addEventListener('click', async event => {

        if(event.isTrusted)

        {

            event.preventDefault();

            await GM.deleteValue('username');

            await GM.deleteValue('password');

            logout.click();

        }

    }, true);

}

/*
 * [ Main ]
 *
 */

document.addEventListener('readystatechange', async () => {

    if(document.readyState !== 'complete') return;

    /*
     * [ Step #0 ] Misc
     *
     * Hide native ads & login notification
     * Trigger submit on advanced search Enter key press from mobile
     * Enhance responsive design :
     * - search pagination
     * - item title
     * - item NFO
     * - item images
     * - item comments
     * - profile page
     *
     */

    const styles = new CSSStyleSheet();
    // language=CSS
    await styles.replace(`
        .ad-alert-wrapper,
        [data-notify="container"],
        [class^="promo-"] {
            display: none !important;
        }
        .pagination {
            flex-wrap: wrap;
        }
        #title {
            word-break: break-word;
        }
        .description-header {
            display: flex;
            flex-wrap: wrap;
            gap: 0.5rem;
        }
        #nfoModal .modal-dialog {
            margin: 0;
            padding: 0.5rem;
            max-width: 100% !important;
        }
        #nfoModal .modal-dialog,
        #nfoModal .modal-content {
            height: 100%;
        }
        #nfoModal .modal-content {
            display: flex;
            flex-direction: column;
        }
        #nfo {
            height: 0;
            flex: 1 1 auto;
            display: flex;
            flex-direction: column;
        }
        #nfo pre {
            flex: 1;
            overflow: auto;
        }
        .content .default img {
            max-width: 100%;
        }
        #add-comment .wysibb {
            min-height: 194px;
        }
        #add-comment .wysibb-toolbar {
            max-height: none !important;
        }
        @media (hover: none) {
            #add-comment .wysibb-toolbar-container > * {
                width: 100%;
            }
        }
        .message {
            padding: 0.5rem !important;
        }
        .add {
            display: flex !important;
            justify-content: space-between;
            align-items: center;
            column-gap: 0.5rem;
            word-break: break-word;
        }
        .right {
            margin: 0 !important;
            padding: 0 !important;
            top: 0 !important;
            border: none !important;
            width: auto !important;
        }
        #comment_text {
            word-break: break-word;
        }
        @media (hover: none) {
            body:has([href*="/user/messages/?action=write&from=inbox&recipient_id="]) h1 div {
                float: none !important;
                margin-top: 1rem;
            }
            body:has([href*="/user/messages/?action=write&from=inbox&recipient_id="]) .content:has(.row) {
                width: 100%;
            }
            body:has([href*="/user/messages/?action=write&from=inbox&recipient_id="]) .content .row {
                flex-direction: column;
            }
            body:has([href*="/user/messages/?action=write&from=inbox&recipient_id="]) .content .row > * {
                max-width: none;
                overflow-x: auto;
            }
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, styles];

    document.querySelector('.input-table[name="name"]')?.setAttribute('enterkeyhint', 'go');

    /*
     * [ Step #1 ] Get login form
     *
     * NOTE : form exists in DOM while logged in
     *
     */

    const form = document.querySelector('#user-login');

    if(!form) return;

    const

        formUsername = form.querySelector('[type=text]'),

        formPassword = form.querySelector('[type=password]');

    /*
     * [ Step #2 ] Handle login & logout
     *
     * Inherent features :
     *
     *  - Save credentials on login
     *
     *  - Delete credentials on logout
     *
     */

    if(getLogout()) createLogoutHandler();

    else form.addEventListener('submit', () => {

        GM.setValue('username', formUsername.value);

        GM.setValue('password', formPassword.value);

        createLogoutHandler();

    });

    /*
     * [Step #3 ] Auto-login
     *
     */

    const

        username = await GM.getValue('username'),

        password = await GM.getValue('password');

    if(username && password && !getLogout())

    {

        formUsername.value = username;

        formPassword.value = password;

        form.querySelector('[type=submit]').click();

    }

});
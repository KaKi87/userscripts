[← Homepage](../README.md)

# Headless recorder

Record browser actions & generate headless browser code.

| [Install](../../../../raw/branch/master/HeadlessRecorder/main.user.js) |
|------------------------------------------------------------------------|
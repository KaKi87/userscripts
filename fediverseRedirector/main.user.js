// ==UserScript==
// @match       https://*/*
// @name        Fediverse redirector
// @description Redirect any Fediverse app to your favourite one
// @grant       GM.getValue
// @grant       GM.setValue
// @grant       GM.registerMenuCommand
// @version     0.2.1
// @author      KaKi87
// @license     MIT
// @namespace   https://git.kaki87.net/KaKi87/userscripts/src/branch/master/fediverseRedirector
// @run-at      document-start
// @require     https://cdn.jsdelivr.net/npm/url-pattern@1.0.3/lib/url-pattern.js
// @require     https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js
// @require     https://cdn.jsdelivr.net/npm/vue@3.3.4/dist/vue.global.prod.js
// ==/UserScript==

const
    APP_ID = 'fediverseRedirector',
    SETTINGS_TITLE = 'Fediverse Redirector — Settings',
    params = new URL(window.location).searchParams,
    settingsKey = `__${APP_ID}__settings`,
    isSettings = !!params.get(settingsKey),
    getSettings = async () => await GM.getValue('settings') || {},
    redirectionKey = `__${APP_ID}__redirection`,
    redirectionData = JSON.parse(decodeURIComponent(params.get(redirectionKey) || 'null')),
    onDom = () => new Promise(resolve => window.addEventListener('DOMContentLoaded', resolve)),
    getRedirectionQuerystring = (
        appId,
        url
    ) => `?${redirectionKey}=${encodeURIComponent(JSON.stringify({
        appId,
        url
    }))}`,
    apps = {
        'mastodon': {
            id: 'mastodon',
            name: 'Mastodon',
            isDetected: () => !!document.querySelector('.app-holder#mastodon'),
            // language=TEXT
            urlPattern: ':clientHostname/@:profileUsername(@:profileHostname)(/:postId)',
            getRedirectionUrlFrom: {
                'mastodon': (hostname, urlParams) => urlParams.postId
                    ? `https://${hostname}/${getRedirectionQuerystring('mastodon', window.location.href)}`
                    : `https://${hostname}/@${urlParams.profileUsername}@${urlParams.profileHostname || urlParams.clientHostname}`,
                'elk': (hostname, urlParams) => urlParams.postId
                    ? urlParams.profileHostname
                        ? `https://${urlParams.clientHostname}/@${urlParams.profileUsername}@${urlParams.profileHostname}/${urlParams.postId}`
                        : `https://${hostname}/${getRedirectionQuerystring('mastodon', `https://${urlParams.clientHostname}/@${urlParams.profileUsername}/${urlParams.postId}`)}`
                    : urlParams.profileHostname
                        ? `https://${hostname}/@${urlParams.profileUsername}${urlParams.profileHostname === hostname ? '' : `@${urlParams.profileHostname}`}`
                        : `https://${hostname}/@${urlParams.profileUsername}${urlParams.clientHostname === hostname ? '' : `@${urlParams.clientHostname}`}`,
                'pixelfed': (hostname, urlParams) => urlParams.profileId || urlParams.postId
                    ? `https://${hostname}/${getRedirectionQuerystring('mastodon', window.location.href)}`
                    : urlParams.profileUsername && `https://${hostname}/@${urlParams.profileUsername}@${urlParams.clientHostname}`
            },
            fetchRedirectionUrl: async () => {
                const requestUrl = new URL('/api/v2/search', window.location.origin);
                requestUrl.searchParams.set('q', redirectionData.url);
                requestUrl.searchParams.set('resolve', 'true');
                const data = Object.values(await (await fetch(requestUrl)).json()).flat()[0];
                return `/@${data['account'] ? `${data['account']['acct']}/${data['id']}` : data['acct']}`;
            }
        },
        'elk': {
            id: 'elk',
            name: 'Elk',
            isDetected: () => !!document.querySelector('meta[property="og:site_name"][content="Elk"]'),
            // language=TEXT
            urlPattern: '/:clientHostname/@:profileUsername(@:profileHostname)(/:postId)',
            getRedirectionUrlFrom: (() => {
                const mastodonOrElk = (hostname, urlParams) => `https://${hostname}/${urlParams.clientHostname}/@${urlParams.profileUsername}${urlParams.profileHostname ? `@${urlParams.profileHostname}` : ''}${urlParams.postId ? `/${urlParams.postId}` : ''}`;
                return {
                    'mastodon': mastodonOrElk,
                    'elk': mastodonOrElk,
                    'pixelfed': (hostname, urlParams) => urlParams.profileId || urlParams.postId
                        ? `https://${hostname}/home/${getRedirectionQuerystring('elk', window.location.href)}`
                        : `https://${hostname}/${urlParams.clientHostname}/@${urlParams.profileUsername}`
                }
            })(),
            fetchRedirectionUrl: () => `https://${localStorage.getItem('elk-current-user-handle').split('@')[1]}/${getRedirectionQuerystring('mastodon', redirectionData.url)}`
        },
        'lemmy': {
            id: 'lemmy',
            name: 'Lemmy',
            isDetected: () => !!unsafeWindow['lemmyConfig'] || !!document.querySelector('.lemmy-site'),
            // language=TEXT
            urlPattern: ':clientHostname/(c/:communityName(@:communityHostname))(u/:profileUsername(@:profileHostname))(post/:postId)(comment/:commentId)',
            getRedirectionUrlFrom: {
                'lemmy': async (hostname, urlParams) => {
                    let url;
                    if(urlParams.communityName)
                        url = `https://${hostname}/c/${urlParams.communityName}@${urlParams.communityHostname || urlParams.clientHostname}`;
                    else if(urlParams.profileUsername)
                        url = `https://${hostname}/u/${urlParams.profileUsername}@${urlParams.profileHostname || urlParams.clientHostname}`;
                    else if(urlParams.postId){
                        await onDom();
                        url = `https://${hostname}/${getRedirectionQuerystring('lemmy', document.querySelector('.post-listing use[*|href$="assets/symbols.svg#icon-fedilink"]')?.parentElement.parentElement.href || window.location.href)}`;
                    }
                    else if(urlParams.commentId){
                        await onDom();
                        url = `https://${hostname}/${getRedirectionQuerystring('lemmy', document.querySelector(`#comment-${urlParams.commentId} use[*|href$="assets/symbols.svg#icon-fedilink"]`).parentElement.parentElement.href)}`;
                    }
                    return url;
                },
                'kbin': (hostname, urlParams) => urlParams.postId
                    ? `https://${hostname}/${getRedirectionQuerystring('lemmy', urlParams.communityHostname ? document.querySelector('[data-action="clipboard#copy"][href^="https"]').href : window.location.href)}`
                    : urlParams.communityName
                        ? `https://${hostname}/c/${urlParams.communityName}@${urlParams.communityHostname || urlParams.clientHostname}`
                        : urlParams.profileUsername
                            ? `https://${hostname}/u/${urlParams.profileUsername}@${urlParams.profileHostname || urlParams.clientHostname}`
                            : undefined,
                'alexandrite': (hostname, urlParams) => urlParams.postId
                    ? `https://${urlParams.clientHostname}/post/${urlParams.postId}`
                    : urlParams.commentId
                        ? `https://${urlParams.clientHostname}/comment/${urlParams.commentId}`
                        : urlParams.communityName
                            ? `https://${hostname}/c/${urlParams.communityName}@${urlParams.communityHostname || urlParams.clientHostname}`
                            : urlParams.profileUsername
                                ? `https://${hostname}/u/${urlParams.profileUsername}@${urlParams.profileHostname || urlParams.clientHostname}`
                                : undefined
            },
            fetchRedirectionUrl: async () => {
                const requestUrl = new URL('/api/v3/resolve_object', window.location.origin);
                requestUrl.searchParams.set('q', redirectionData.url);
                requestUrl.searchParams.set('auth', Cookies.get('jwt'));
                const
                    data = await (await fetch(requestUrl)).json(),
                    key = Object.keys(data)[0];
                return `/${key}/${data[key][key]['id']}`;
            }
        },
        'pixelfed': {
            id: 'pixelfed',
            name: 'Pixelfed',
            isDetected: () => !!unsafeWindow['pixelfed'],
            // language=TEXT
            urlPattern: ':clientHostname/(:profileUsername)(i/web/profile/:profileId)(p/:profileUsername/:postId)(i/web/post/:postId)',
            redirectUrlPath: '/i/web',
            getRedirectionUrlFrom: {
                'pixelfed': hostname => `https://${hostname}/i/web/${getRedirectionQuerystring('pixelfed', window.location.href)}`,
                'mastodon': (hostname, urlParams) => urlParams.profileHostname
                    ? urlParams.postId
                        ? undefined
                        : `https://${hostname}/i/web/${getRedirectionQuerystring('pixelfed', `https://${urlParams.profileHostname}/@${urlParams.profileUsername}`)}`
                    : `https://${hostname}/i/web/${getRedirectionQuerystring('pixelfed', window.location.href)}`,
                'elk': (hostname, urlParams) => urlParams.postId
                    ? urlParams.profileHostname
                        ? `https://${urlParams.clientHostname}/@${urlParams.profileUsername}@${urlParams.profileHostname}/${urlParams.postId}`
                        : `https://${hostname}/i/web/${getRedirectionQuerystring('pixelfed', `https://${urlParams.clientHostname}/@${urlParams.profileUsername}/${urlParams.postId}`)}`
                    : urlParams.profileHostname
                        ? `https://${hostname}/@${urlParams.profileUsername}${urlParams.profileHostname === hostname ? '' : `@${urlParams.profileHostname}`}`
                        : `https://${hostname}/@${urlParams.profileUsername}${urlParams.clientHostname === hostname ? '' : `@${urlParams.clientHostname}`}`
            },
            fetchRedirectionUrl: async () => {
                const requestUrl = new URL('/api/v2/search', window.location.origin);
                requestUrl.searchParams.set('q', redirectionData.url);
                requestUrl.searchParams.set('resolve', 'true');
                const data = Object.values(await (await fetch(
                    requestUrl,
                    {
                        headers: {
                            'x-xsrf-token': Cookies.get('XSRF-TOKEN')
                        }
                    }
                )).json()).flat()[0];
                return `/i/web/${data['account'] ? `post` : `profile`}/${data['id']}`;
            }
        },
        'kbin': {
            id: 'kbin',
            name: 'Kbin',
            isDetected: () => document.body?.getAttribute('data-controller')?.startsWith('kbin'),
            // language=TEXT
            urlPattern: ':clientHostname/(d/:instanceHostname(/*))(m/:communityName(@:communityHostname)(/t/:postId(/:postSlug))(/comment/:commentId)(/*))(u/(@):profileUsername(@:profileHostname)(/*))',
            getRedirectionUrlFrom: {
                'kbin': (hostname, urlParams) => {
                    const urlMain = urlParams.instanceHostname
                        ? `https://${hostname}/d/${urlParams.instanceHostname}`
                        : urlParams.communityName
                            ? urlParams.postId || urlParams.commentId
                                ? undefined // FIXME: https://codeberg.org/Kbin/kbin-core/issues/497
                                : `https://${hostname}/m/${urlParams.communityName}@${urlParams.communityHostname || urlParams.clientHostname}`
                            : urlParams.profileUsername
                            && `https://${hostname}/u/@${urlParams.profileUsername}@${urlParams.profileHostname || urlParams.clientHostname}`;
                    return urlMain ? `${urlMain}${urlParams._ ? `/${urlParams._}` : ''}` : undefined;
                },
                'lemmy': (hostname, urlParams) => urlParams.postId || urlParams.commentId
                    ? undefined // FIXME: https://codeberg.org/Kbin/kbin-core/issues/497
                    : `https://${hostname}/${urlParams.communityName ? `m/${urlParams.communityName}@${urlParams.communityHostname || urlParams.clientHostname}` : ''}${urlParams.profileUsername ? `u/@${urlParams.profileUsername}@${urlParams.profileHostname || urlParams.clientHostname}` : ''}`
            }
        },
        'alexandrite': {
            id: 'alexandrite',
            name: 'Alexandrite',
            isDetected: () => document.querySelector('[href="https://github.com/sheodox/alexandrite"]'),
            // language=TEXT
            urlPattern: '/:clientHostname/(c/:communityName(@:communityHostname))(u/:profileUsername(@:profileHostname))(post/:postId)(comment/:commentId)',
            getRedirectionUrlFrom: (() => {
                const lemmyOrAlexandrite = (hostname, urlParams) => `https://${hostname}/${urlParams.clientHostname}/${urlParams.communityName ? `c/${urlParams.communityName}${urlParams.communityHostname ? `@${urlParams.communityHostname}` : ''}` : ''}${urlParams.profileUsername ? `u/${urlParams.profileUsername}${urlParams.profileHostname ? `@${urlParams.profileHostname}` : ''}` : ''}${urlParams.postId ? `post/${urlParams.postId}` : ''}${urlParams.commentId ? `comment/${urlParams.commentId}` : ''}`;
                return {
                    'lemmy': lemmyOrAlexandrite,
                    'alexandrite': lemmyOrAlexandrite
                };
            })()
        }
    };
if(isSettings) (async () => {
    if(document.readyState === 'loading')
        await onDom();
    document.head.innerHTML = `
        <title>${SETTINGS_TITLE}</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="darkreader-lock">
    `;
    unsafeWindow.Vue = Vue;
    const settingsApp = Vue.createApp({
        data: () => ({
            apps: Object.values(apps),
            appSettings: {},
            isSavedActive: false
        }),
        computed: {
            appPlaceholder: () => ({
                'mastodon': 'mamot.fr',
                'elk': 'main.elk.zone',
                'lemmy': 'sh.itjust.works',
                'pixelfed': 'pixelfed.de',
                'kbin': 'kbin.projectsegfau.lt',
                'alexandrite': 'alexandrite.app'
            }),
            otherApps: function(){
                return this.apps.reduce((
                    otherApps,
                    app
                ) => ({
                    ...otherApps,
                    [app.id]: this.apps
                        .filter(otherApp => otherApp.id !== app.id && otherApp.getRedirectionUrlFrom[app.id])
                        .map(otherApp => ({
                            ...otherApp,
                            isDisabled: this.appSettings[otherApp.id].type !== 'instance'
                        }))
                }), {});
            }
        },
        methods: {
            load: async function(){
                const settings = await getSettings();
                this.appSettings = this.apps.reduce((
                    appSettings,
                    app
                ) => ({
                    ...appSettings,
                    [app.id]: {
                        ...settings[app.id],
                        type: settings[app.id]?.type || (settings[app.id]?.hostname ? 'instance' : 'none'),
                    }
                }), {});
            },
            submit: async function(){
                await GM.setValue(
                    'settings',
                    this.appSettings
                );
                await this.load();
                this.isSavedActive = true;
                await new Promise(resolve => setTimeout(resolve, 2000));
                this.isSavedActive = false;
            }
        },
        mounted: async function(){
            await this.load();
        },
        watch: {
            appSettings: {
                deep: true,
                handler: function(){
                    for(const item of Object.values(this.appSettings)){
                        switch(item.type){
                            case 'none': {
                                item.hostname = '';
                                item.app = '';
                                break;
                            }
                            case 'instance': {
                                item.app = '';
                                break;
                            }
                            case 'app': {
                                item.hostname = '';
                                if(item.app && this.appSettings[item.app].type !== 'instance')
                                    item.app = '';
                                break;
                            }
                        }
                    }
                }
            }
        },
        template: `
            <h1>${SETTINGS_TITLE}</h1>
            <form
                class="app__form"
                @submit.prevent="submit"
            >
                <template
                    v-for="app in apps"
                ><fieldset
                    v-if="appSettings[app.id]"
                    class="app__form__app"
                >
                    <legend>{{ app.name }} redirection</legend>
                    <label
                        class="app__form__app__redirectionType"
                    ><input
                        v-model="appSettings[app.id].type"
                        :name="\`appSettings[\${app.id}].type\`"
                        type="radio"
                        value="none"
                    >None</label>
                    <label
                        class="app__form__app__redirectionType"
                    >
                        <input
                            v-model="appSettings[app.id].type"
                            :name="\`appSettings[\${app.id}].type\`"
                            type="radio"
                            value="instance"
                        >
                        Specific instance
                        <input
                            v-model="appSettings[app.id].hostname"
                            type="text"
                            :placeholder="appPlaceholder[app.id]"
                            :required="appSettings[app.id].type === 'instance'"
                            :disabled="appSettings[app.id].type !== 'instance'"
                        >
                    </label>
                    <label
                        class="app__form__app__redirectionType"
                    >
                        <input
                            v-model="appSettings[app.id].type"
                            :name="\`appSettings[\${app.id}].type\`"
                            type="radio"
                            value="app"
                            :disabled="!otherApps[app.id].length"
                        >
                        Other app
                        <select
                            v-model="appSettings[app.id].app"
                            :required="appSettings[app.id].type === 'app'"
                            :disabled="appSettings[app.id].type !== 'app'"
                        >
                            <option value="" disabled></option>
                            <option
                                v-for="otherApp in otherApps[app.id]"
                                :value="otherApp.id"
                                :disabled="otherApp.isDisabled"
                            >{{ otherApp.name }}</option>
                        </select>
                    </label>
                </fieldset></template>
                <footer class="app__form__footer">
                    <input
                        ref="app__form__footer__submit"
                        type="submit"
                        value="Submit"
                    >
                    <p
                        class="app__form__footer__saved"
                        :class="{ 'app__form__footer__saved--active': isSavedActive }"
                    >Settings saved.</p>
                </footer>
            </form>
        `
    });
    settingsApp.mount(document.body);
    const settingsStyles = new CSSStyleSheet();
    await settingsStyles.replace(`
        :root {
            color-scheme: dark;
        }
        body {
            font-family: sans-serif;
        }
        input {
            margin: 0;
        }
        .app__form,
        .app__form__app,
        .app__form__footer {
            display: flex;
            row-gap: 1rem;
        }
        .app__form,
        .app__form__app {
            flex-direction: column;
        }
        .app__form {
            align-items: flex-start;
        }
        .app__form__app {
            padding: 1rem;
        }
        .app__form__app__redirectionType,
        .app__form__footer {
            column-gap: 0.5rem;
        }
        .app__form__app__redirectionType {
            display: grid;
            grid-template-columns: auto 1fr 2fr;
            align-items: center;
            column-gap: 1rem;
        }
        .app__form__footer {
            align-items: center;
        }
        .app__form__footer__saved {
            margin: 0;
            display: none;
        }
        .app__form__footer__saved--active {
            display: block;
        }
    `);
    document.adoptedStyleSheets = [...document.adoptedStyleSheets, settingsStyles];
})().catch(console.error);
else if(redirectionData) (async () => {
    window.stop();
    window.location.replace(await apps[redirectionData.appId].fetchRedirectionUrl());
})().catch(console.error);
else {
    const
        observer = new MutationObserver(async () => {
            const app = Object.values(apps).find(({ isDetected }) => isDetected());
            if(app){
                observer.disconnect();
                const urlParams = new UrlPattern(app.urlPattern, { segmentValueCharset: 'a-zA-Z0-9._-' })
                    .match(`${app.urlPattern.startsWith('/') ? '' : window.location.hostname}${window.location.pathname}`);
                if(!urlParams) return;
                const settings = await getSettings();
                let url;
                switch(settings[app.id].type){
                    case 'instance': {
                        if(window.location.hostname === settings[app.id].hostname) return;
                        url = await apps[app.id].getRedirectionUrlFrom[app.id](settings[app.id].hostname, urlParams);
                        break;
                    }
                    case 'app': {
                        url = await apps[settings[app.id].app].getRedirectionUrlFrom[app.id](settings[settings[app.id].app].hostname, urlParams);
                        break;
                    }
                }
                if(url){
                    await new Promise(resolve => setTimeout(resolve, 0));
                    window.stop();
                    window.location.replace(url);
                }
            }
        });
    observer.observe(
        document.documentElement,
        {
            childList: true,
            subtree: true
        }
    );
    document.addEventListener('load', () => observer.disconnect());
}

GM.registerMenuCommand(
    'Settings',
    () => window.open(
        `https://example.com?${settingsKey}=1`,
        '_blank',
        'toolbar=no,width=550,height=550'
    )
);
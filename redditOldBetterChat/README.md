[← Homepage](../README.md)

# Enhancements for Reddit Chat

Responsive design & notifications for Reddit chat.

| [Install](../../../../raw/branch/master/redditOldBetterChat/main.user.js) |
|---------------------------------------------------------------------------|

## DEPRECATION NOTICE

This project is now deprecated in favor of [Responsive design for Reddit Old](../redditOldResponsive/README.md).